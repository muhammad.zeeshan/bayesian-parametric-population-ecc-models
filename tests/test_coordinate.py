from pop_models.coordinate import *
import numpy

seed = 4
random_state = numpy.random.RandomState(seed)
n_samples = 1000

def test_transformations():
    A_coord = Coordinate("A")
    B_coord = Coordinate("B")
    C_coord = Coordinate("C")
    D_coord = Coordinate("D")
    E_coord = Coordinate("E")

    A_data = random_state.uniform(-1.0, 1.0, n_samples)
    B_data = random_state.uniform(-1.0, 1.0, n_samples)
    C_data = random_state.uniform(-1.0, 1.0, n_samples)
    D_data = random_state.uniform(-1.0, 1.0, n_samples)
    E_data = random_state.uniform(-1.0, 1.0, n_samples)

    def AB_to_CD(AB):
        a, b = AB
        c, d = a + b, a - b
        return [c, d]

    def A_to_E(A):
        a, = A
        e = -a
        return [e]

    transformations = CoordinateTransforms({
        (
            CoordinateSystem(A_coord, B_coord),
            CoordinateSystem(C_coord, D_coord),
        ) : AB_to_CD,
        (
            CoordinateSystem(A_coord),
            CoordinateSystem(E_coord),
        ) : A_to_E,
    })

    expected_C_data_from_AB = A_data + B_data
    expected_D_data_from_AB = A_data - B_data
    expected_E_data_from_A = -A_data

    # Trivial test: does a hand-entered transformation still work?
    actual_C_data_from_AB, actual_D_data_from_AB = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(C_coord, D_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)

    # Reordering test: can it re-order a hand-entered transformation's inputs?
    actual_C_data_from_AB, actual_D_data_from_AB = transformations[
        CoordinateSystem(B_coord, A_coord),
        CoordinateSystem(C_coord, D_coord),
    ]([B_data, A_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)

    # Reordering test: can it re-order a hand-entered transformation's outputs?
    actual_D_data_from_AB, actual_C_data_from_AB = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(D_coord, C_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)

    # Reordering test: can it re-order both inputs and outputs?
    actual_D_data_from_AB, actual_C_data_from_AB = transformations[
        CoordinateSystem(B_coord, A_coord),
        CoordinateSystem(D_coord, C_coord),
    ]([B_data, A_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)

    # Trivial truncation test: can it downselect the input coordinates?
    actual_A_data_from_AB, = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(A_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_A_data_from_AB, A_data)

    # Truncation tests: can it drop unwanted target coordinates?
    actual_C_data_from_AB, = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(C_coord),
    ]([A_data, B_data])
    actual_D_data_from_AB, = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(D_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)

    # Combination test: can it combine multiple transformations
    (
        actual_C_data_from_AB, actual_D_data_from_AB, actual_E_data_from_A,
    ) = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(C_coord, D_coord, E_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)
    assert numpy.allclose(actual_E_data_from_A, expected_E_data_from_A)

    # Combination and truncation tests: can it combine multiple transformations
    # and then truncate?
    (
        actual_C_data_from_AB, actual_E_data_from_A,
    ) = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(C_coord, E_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_C_data_from_AB, expected_C_data_from_AB)
    assert numpy.allclose(actual_E_data_from_A, expected_E_data_from_A)
    (
        actual_D_data_from_AB, actual_E_data_from_A,
    ) = transformations[
        CoordinateSystem(A_coord, B_coord),
        CoordinateSystem(D_coord, E_coord),
    ]([A_data, B_data])
    assert numpy.allclose(actual_D_data_from_AB, expected_D_data_from_AB)
    assert numpy.allclose(actual_E_data_from_A, expected_E_data_from_A)


    ## FAILURE: Added feature should catch this
    # Combine a known transformation with a simple carry-over of a coordinate.
    # (
    #     actual_C_data_from_AC, actual_E_data_from_AC,
    # ) = transformations[
    #     CoordinateSystem(A_coord, C_coord),
    #     CoordinateSystem(C_coord, E_coord),
    # ]([A_data, C_data])
    # assert numpy.allclose(actual_C_data_from_AC, C_data)
    # assert numpy.allclose(actual_E_data_from_AC, expected_E_data_from_A)
