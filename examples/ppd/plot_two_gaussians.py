r"""
Gaussian PPDs
=============

Demonstrates the posterior predictive distribution (PPD) of a Gaussian
distribution with an unknown mean :math:`\mu`,
:math:`p(x\mid\mu) = \mathcal{N}(x;\mu,\sigma)`, for which we have drawn samples
from the posterior distribution, which is itself Gaussian:
:math:`p(\mu\mid\mathrm{data}) = \mathcal{N}(\mu;\mu_0,\sigma_0)`.

This example is useful because the PPD is itself analytical, and can be compared
to the estimate from samples.

.. math::
   p(x\mid\mathrm{data}) =
   \int_{-\infty}^{+\infty} p(x\mid\mu) p(\mu\mid\mathrm{data}) \mathrm{d}\mu =
   \mathcal{N}(x\mid\mu_0,\sqrt{\sigma^2+\sigma_0^2})
"""

print("SKIPPING")

'''
from __future__ import division, print_function

import numpy
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats

import pop_models.ppd

## RNG seed ##
seed = 1


## Plotting parameters ##
plot_min = -10.0
plot_max = +10.0
n_smooth = 150


## Free parameters ##
mu_0 = 0.0
sigma = 1.0
sigma_0 = 1.0


## Sample sizes ##
n_post = 20000
n_ppd = 20000


# Random state
random = numpy.random.RandomState(seed)


# What we should get for the stdev of the PPD
sigma_ppd = numpy.sqrt(sigma**2 + sigma_0**2)


# x values to plot
plot_x = numpy.linspace(plot_min, plot_max, n_smooth)


# Posterior samples
mu_samples = scipy.stats.norm(mu_0, sigma).rvs((n_post,1), random_state=random)

# PPD samples
def sampler(N, mu, random_state=None):
    return scipy.stats.norm(mu, sigma_0).rvs((N,1), random_state=random_state)

ppd_samples = pop_models.ppd.sample_ppd(
    sampler, n_ppd,
    mu_samples,
    random_state=random,
).flatten()



# PPDs
ppd_analytic = scipy.stats.norm.pdf(plot_x, mu_0, sigma_ppd)
ppd_sampled = scipy.stats.gaussian_kde(ppd_samples, bw_method="scott")(plot_x)


fig, ax = plt.subplots(figsize=[4,4])

ax.plot(
    plot_x, ppd_analytic,
    color="black",
    label="Analytic",
)

ax.plot(
    plot_x, ppd_sampled,
    color="#d62728",
    label="Sampled",
)


ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$p(x\mid\mathrm{data})$")

ax.legend(loc="best")


plt.show()
'''
