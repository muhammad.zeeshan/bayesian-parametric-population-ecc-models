r"""
Gaussian population 3 (mixture on grid with selection bias)
===================

Third in a sequence of trivial  examples of using the PopModels population inference framework; return to the installation page to get started.
Building on the first two examples, which had a unimodal population, this example shows how to use a mixture model population and infer its parameters, via  
As in previous demonstrations, this example also shows how to use the  SensitiveVolume and MonteCarloVolumeIntegralPoissonMean classes , s well as continuing to demonstrate how to use  Detection, Population, PoissonMean, ReweightedPosteriorsDetectionLikelihood, log_posterior,  and the Coordinate and CoordinateSystem framework.    


This example consists of a mixture of two populations.  Each population has  one property ("mass" or "x"), whose event rate and "mass distribution" parameters will be determined by fair draws.  Each x distribution is assumed to be gaussian distributed with unknown mean and (known) unit variance.  Inference assumes a uniform prior in 'x' and rate.

The plot produced at the end shows
  * Top panel: Fair draws of x (thin lines); true 'x' distribution; and selection function.
  * Corner plot: The inferred mass distribution and rate distribution (as one-dimensional marginal distributions), superposed with the likleihood.


In this example, PopModels is used for  *grid-based* inference: no MCMC (or Monte Carlo integral) is performed.
Instead, the likelihood is evaluated on a grid; marginal posteriors follow by direct quadrature.


The code  works as in the previous example (Gaussian population 1).  Only key differences are highlighted below
  * Define the population (```GaussianPopuolation```)
    * Define a mixture (```MixturePopulation```), as two instances of the gaussian population. Coordinates acquire postfix labels to differentiate them
  * Define the sensitive volume (```PiecewiseSensitiveVolume```). 
  * Create a tool which computes the expected number of events using the sensitive volume and the population (```MonteCarloVolumeIntegralPoissonMean```)
  * Draw from the population (accounting for the survey's sensitive volume)
  * Define the likleihood function and log posterior
  * Evaluate the likelihood on a grid
  * Compute marginal distributions by quadrature

TODO
"""

import numpy
import matplotlib
matplotlib.use("Agg")
from matplotlib import gridspec
import matplotlib.pyplot as plt
import scipy.stats

from pop_models.coordinate import Coordinate, CoordinateSystem
from pop_models.detection import (
    Detection, ReweightedPosteriorsDetectionLikelihood,
)
from pop_models.poisson_mean import MonteCarloVolumeIntegralPoissonMean
from pop_models.population import Population, MixturePopulation
from pop_models.posterior import PosteriorGrid, PopulationInferenceGrid
from pop_models.prob import sample_with_cond
from pop_models.volume import SensitiveVolume


# Construct simple coordinate system from scratch
X = Coordinate("x")
coord_system = CoordinateSystem(X)

# Names of the parameters of the base population.  Actual parameter names will
# have mixture component suffixes appended automatically.
param_names_base = ("rate",)

class GaussianPopulation(Population):
    """
    Defines a simple population with a single observable ("x"), which obey a
    Gaussian distribution with unknown mean "mean" and known unit variance.
    Normalization of this distribution is given by an unknown parameter "rate".
    """
    def __init__(self, mean, sigma):
        # No special information needed, just pass the CoordinateSystem and
        # parameter names to the parent's constructor method.
        super().__init__(coord_system, param_names_base)

        self.mean = mean
        self.sigma = sigma

        self.sigma_inv = 1.0 / sigma
        self.sigma_inv_sq = sigma**-2.0


    def pdf(self, observables, parameters, where=True, **kwargs):
        # First argument is a list of observables, but there's only one, "x", so
        # we pull that out of the list.
        x, = observables

        # Only parameter is the rate, use it to determine the output shape.
        params_shape = numpy.shape(parameters["rate"])
        # Output array should have the same shape as the parameters, plus an
        # additional axis with size 'n_samples', so the result is 'n_samples'
        # draws for each parameter value.
        output_shape = params_shape + numpy.shape(x)

        # PDF is a normal distribution with unit variance.  This is the
        # functional form for such a distribution.  Note that by using
        # `numpy.subtract.outer(mean, x)` instead of `mean - x`, we compute
        # all combinations of `mean[i_1,...,i_n] - x[j_1,...,j_m]`, and the
        # shape of the output array is `numpy.shape(mean) + numpy.shape(x)`.
        # Handling array shapes like this is a requirement of the PopModels API.
        delta = self.mean - x
        p = (
            numpy.exp(-0.5*numpy.square(delta)*self.sigma_inv_sq) /
            numpy.sqrt(2.0*numpy.pi) * self.sigma_inv
        )

        return numpy.broadcast_to(p, output_shape)


    def rvs(
            self,
            n_samples, parameters,
            where=True, random_state=None, **kwargs
        ):
        if random_state is None:
            random_state = numpy.random

        mean = self.mean
        stdev = self.sigma

        # Only parameter is the rate, use it to determine the output shape.
        params_shape = numpy.shape(parameters["rate"])
        # Output array should have the same shape as the parameters, plus an
        # additional axis with size 'n_samples', so the result is 'n_samples'
        # draws for each parameter value.
        output_shape = params_shape + (n_samples,)

        x_samples = random_state.normal(
            loc=mean, scale=stdev,
            size=output_shape,
        )

        # Output needs to be formatted as a list of samples for each observable.
        # Since 'x' is the only observable, there's only one entry in this list.
        return [x_samples]


    def normalization(self, parameters, where=True, **kwargs):
        # The normalization of the intensity function is just the "rate"
        # parameter.
        return parameters["rate"]


class PiecewiseSensitiveVolume(SensitiveVolume):
    """
    We allow for a survey whose sensitivity covers an area defined by two
    Gaussians, with the normalization, relative weights, locations, and widths
    all configurable.  To demonstrate the importance of selection bias, we make
    sure not to set these such that they're symmetric about the population's
    true mean, as the result would be artificially cancelling out the selection
    biases.
    """
    def __init__(
            self,
            obs_time,
            cutoff_low, cutoff_mid, cutoff_high,
            weight1, weight2,
        ):
        # Parent class needs to know the coordinate system we're working in.
        super().__init__(coord_system)

        self.obs_time = obs_time

        self.cutoff_low = cutoff_low
        self.cutoff_mid = cutoff_mid
        self.cutoff_high = cutoff_high

        self.weight1 = weight1
        self.weight2 = weight2

    def __call__(self, observables):
        x, = observables

        in_low_range = (x >= self.cutoff_low) & (x < self.cutoff_mid)
        in_high_range = (x >= self.cutoff_mid) & (x <= self.cutoff_high)

        return numpy.piecewise(
            x,
            [in_low_range, in_high_range],
            [self.weight1, self.weight2],
        )


class SimpleDetection(Detection):
    """
    We assume detections with perfect measurements, so all posterior samples are
    of the correct value, and if the `likelihood` method were implemented, it
    would be a delta function at the true value (which would not integrate well
    numerically, so we must use `posterior_samples` since that makes a Monte
    Carlo integral perfectly adaptive).
    """
    def __init__(self, x_true):
        # Parent constructor needs to know the coordinate system.
        super().__init__(coord_system)
        # In addition, we need to know the true value.
        self.x_true = x_true

    def posterior_samples(self, n_samples, random_state=None):
        # This method returns a tuple of two things:
        # The first is posterior samples for a list of observables, and in this
        # case we have a single observable (x), so the list has one element. The
        # posterior samples all have the object's true value, as we've assumed
        # perfect measurement error, so we simply return an array with the
        # desired number of samples (`n_samples`), where every element is the
        # true value of "x".
        # The second element of the tuple must either be the prior of each
        # posterior sample, or `None` if the prior is uniform.  Since the prior
        # doesn't matter in the case of a delta-function likelihood, we just set
        # it to uniform for simplicity's sake.
        return [numpy.repeat(self.x_true, n_samples)], None


# Seed the RNG for reproducability
seed = 15
random = numpy.random.RandomState(seed)

# Set the observing time for the "survey"
obs_time = 100.0


# Constants for the population.  We assume we know the precise shape of the
# population distribution, and need only infer the overall normalization and
# the mixture weight.
mean_low, sigma_low = -3.0, 1.0
mean_high, sigma_high = +3.0, 1.0

# Set the true values for the parameters of the population, and also store them
# in a Parameters dict.
rate_true = 2.0
mixture_frac_true = 0.7

rate_low_true = rate_true * mixture_frac_true
rate_high_true = rate_true * (1.0 - mixture_frac_true)

parameters_true = {
    "rate_low": numpy.asarray([rate_low_true]),
    "rate_high": numpy.asarray([rate_high_true]),
}

# Set the parameters of the survey volume.
volume_cutoff_low = -10.0
volume_cutoff_mid = 0.0
volume_cutoff_high = +10.0
volume_weight1 = 30.0
volume_weight2 = 70.0

# Set the number of points to use in plotting and evaluation
n_plotting = 500
n_grid = (30, 30)

# Set the bounds for plotting and evaluation.
x_min_plotting = -10.0
x_max_plotting = +10.0

rate_low_min_grid = 0.75
rate_low_max_grid = 2.75

rate_high_min_grid = 0.20
rate_high_max_grid = 1.20


# Create an array of 'x' values for plotting the intensity function.
x_plotting = numpy.linspace(x_min_plotting, x_max_plotting, n_plotting)

# 1- and 2-D grids of the unknown parameters, as well as a Parameters dict
# mapping each variable to its 2-D grid.
rate_low_grid, rate_low_dx = numpy.linspace(
    rate_low_min_grid, rate_low_max_grid, n_grid[0],
    retstep=True,
)
rate_high_grid, rate_high_dx = numpy.linspace(
    rate_high_min_grid, rate_high_max_grid, n_grid[1],
    retstep=True,
)

# Initialize the Population and PoissonMean objects.
sub_populations = {
    "_low": GaussianPopulation(mean_low, sigma_low),
    "_high": GaussianPopulation(mean_high, sigma_high),
}
population = MixturePopulation(coord_system, sub_populations)
volume = PiecewiseSensitiveVolume(
    obs_time,
    volume_cutoff_low, volume_cutoff_mid, volume_cutoff_high,
    volume_weight1, volume_weight2,
)
expval = MonteCarloVolumeIntegralPoissonMean(population, volume)

# Compute \mu for the true values -- this tells us the average number of
# detections we should make in our survey.
expval_true = expval(parameters_true)[0] # type: ignore

# Take one random (seeded for reproducibility) Poisson draw for the number of
# detections, and then draw that many random (seeded) true values of objects
# detected from the population.  Randomly keep samples according to the
# probability `volume(x) / max(volume)` to introduce selection bias.
n_detections = scipy.stats.poisson(expval_true).rvs(1, random_state=random)[0]

def population_sampler(n_samples):
    samples = population.rvs(n_samples, parameters_true, random_state=random)
    x, = samples
    return x[0]

volume_max = max(volume([volume_cutoff_low]), volume([volume_cutoff_high]))
def relative_volume(observables):
    return volume(observables) / volume_max

def accept_with_prob(x):
    observables = (x,)

    p = relative_volume(observables)

    return random.uniform(size=numpy.shape(x)) < p


x_truths = sample_with_cond(
    population_sampler, shape=n_detections, cond=accept_with_prob,
)

# PopModels needs a list of `DetectionLikelihood` objects.  We have already
# defined a `SimpleDetection` class (child of the `Detection` class), which
# implements the `posterior_samples` method.  The
# `ReweightedPosteriorsDetectionLikelihood` class is a subclass of
# `DetectionLikelihood`, which knows how to use a `Detection` object that's
# implemented the `posterior_samples` method, so it's the perfect fit.  We
# create a list of `DetectionLikelihood` objects to pass to PopModels' API.
detections = [SimpleDetection(x_true) for x_true in x_truths]
detection_likelihoods = [
    ReweightedPosteriorsDetectionLikelihood(population, detection)
    for detection in detections
]

# Define the `log_prior` function.
def log_prior(parameters, *args, **kwargs):
    """
    Assume a prior uniform in rate and mean.
    """
    rate_low = parameters["rate_low"]
    return numpy.zeros_like(rate_low)



# Create posterior grid object
variables = {"rate_low" : rate_low_grid, "rate_high" : rate_high_grid}
variable_dxs = {"rate_low" : rate_low_dx, "rate_high" : rate_high_dx}
post_grid = PosteriorGrid(
    variables,
    population.param_names,
    variable_dxs=variable_dxs,
)

# Create population inference object
pop_inference = PopulationInferenceGrid(
    post_grid,
    expval, detection_likelihoods, log_prior,
    vectorize_grid_eval=True,
)

# Plot the population
fig_pop, ax_pop = plt.subplots(figsize=(6,4))

# Plot the population's intensity function.
rho_x = population.intensity((x_plotting,), parameters=parameters_true)
ax_pop.plot(
    x_plotting, rho_x[0], # type: ignore
    label=(
        r"$\rho(x | "
          r"\mathcal{R}_{\mathrm{low,true}}, "
          r"\mathcal{R}_{\mathrm{high,true}}"
        r")$"
    ),
)
# Plot the sensitive volume.
ax_pop.plot(
    x_plotting,
    volume((x_plotting,)) / obs_time,
    label=r"$V(x) / T_{\mathrm{obs}}$",
)
_, ax_pop_ymax = ax_pop.get_ylim()

# Plot the detected events, and where they lie on the population.
for i, x_truth in enumerate(x_truths):
    label = "Detections" if i == 0 else None
    ax_pop.plot(
        [x_truth, x_truth], [0.0, 0.1*ax_pop_ymax],
        color="black", linestyle="solid", alpha=0.4,
        label=label,
    )

ax_pop.set_xlabel(r"$x$")
ax_pop.set_ylabel(r"$\rho(x | \Lambda_{\mathrm{true}})$")

ax_pop.legend(loc="best")

fig_pop.show()

################################################################################

# Make a corner plot of the posteriors
fig_post = post_grid.plot_corner(levels=20, truths=parameters_true)

fig_post.show()
