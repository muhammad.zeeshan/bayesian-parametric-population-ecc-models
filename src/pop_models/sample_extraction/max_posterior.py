def compute_burnin(log_post, n_dim, retall=False):
    r"""
    Computes the burnin of an ensemble, looking at the chain of log-posteriors
    ``log_post`` (with shape ``(n_samples, n_walkers)``), by finding the first
    point in each chain where it comes within ``n_dim / 2`` of the maximum
    log-posterior across all chains.  The number of iterations the worst walker
    takes to reach this point is taken as the burn-in time for *all* chains.
    """
    import numpy

    # Threshold is the max value across all walkers and samples, minus half the
    # number of dimensions in the parameter space.
    log_post_thresh = numpy.max(log_post) - 0.5*n_dim

    # Check which samples are above the threshold, use argmax to find the index
    # of the first sample to do this for each walker, and then use max to get
    # the index from the slowest walker.
    n_burnins = numpy.argmax(
        log_post > log_post_thresh,
        axis=0,
    )
    n_burnin = numpy.max(n_burnins)

    if retall:
        return n_burnin, n_burnins
    else:
        return n_burnin


def plot_burnins(n_burnins):
    ## TODO: make sure range never goes lower than zero, and always has some
    ## width, in event of zero burnin.
    import numpy
    import matplotlib
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()

    ax.yaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(integer=True, min_n_ticks=1)
    )

    # Count the occurrences of each 'n', from 0 up to the largest value that
    # occurs, and then discard the counts before the first occurrence.
    counts = numpy.bincount(n_burnins)
    n_first = numpy.argmax(counts > 0)
    n_last_plus_one = numpy.size(counts)
    counts = counts[n_first:]

    ns = numpy.arange(n_first, n_last_plus_one)

    ax.bar(ns, height=counts)

    ax.set_xlabel("# burnin samples")
    ax.set_ylabel("# walkers")

    return fig, ax
