import itertools
import typing
import numpy
from pop_models.posterior import H5RawPosteriorSamples

__colors = [
    "#1f77b4", "#ff7f0e",
    "#2ca02c", "#d62728",
    "#9467bd", "#8c564b",
    "#e377c2", "#7f7f7f",
    "#bcbd22", "#17becf",
]
colors = itertools.cycle(__colors)

__linestyles = (
    ["solid"] * 10 +
    ["dashed"] * 10 +
    ["dotted"] * 10
)
linestyles = itertools.cycle(__linestyles)


def acorr(X, lag, axis=0):
    """
    Computes the auto-correlation of the sequence ``X`` given a lag ``lag``.
    """
    import numpy

    N = numpy.shape(X)[axis]
    dX = X - numpy.mean(X, axis=axis)

    result = numpy.sum(dX[lag:] * dX[:N-lag], axis=axis)
    result /= numpy.sum(numpy.square(dX), axis=axis)

    return result


_supported_acorr_methods = ("default", "max_likelihood")
def acorr_time(
        chains,
        tolerance=0.1,
        initial_grid_size=100, max_retries=10,
        method="default",
        return_acorr=False,
    ):
    import numpy

    from ..utils import bisect_int



    n_samples, n_walkers, n_dim = numpy.shape(chains)

    # Initialize array that will hold the auto-correlation for each lag.
    # We will not necessarily evaluate it at every point, but we need to
    # initialize the array, so we fill it with nan's.
    acorrs = numpy.tile(numpy.nan, (n_samples, n_walkers, n_dim))

    def ac(lag, w, d):
        # Auto-correlation hasn't been computed yet, so compute it and store it.
        if numpy.all(numpy.isnan(acorrs[lag])):
            acorrs[lag] = acorr(chains, lag)

        # Auto-correlation is defined, and has been tabulated (possibly during
        # this function call) so return it.
        return acorrs[lag,w,d]

    # Evaluate the auto-correlation on an initial coarse grid.
    # We do this mainly for plotting reasons and sanity checks.
    lag_coarse = numpy.logspace(
        numpy.log2(1), numpy.log2(n_samples-1), initial_grid_size,
        base=2, dtype=int,
    )
    for lag in lag_coarse:
        ac(lag, 0, 0)

    lag_coarse = numpy.linspace(0, n_samples-1, initial_grid_size, dtype=int)
    for lag in lag_coarse:
        ac(lag, 0, 0)

    del lag_coarse


    lags = numpy.tile(0, (n_walkers, n_dim))

    for d in range(n_dim):
        for w in range(n_walkers):
            is_converged = False
            lag = n_samples + 1

            lower_bound = 0
            upper_bound = n_samples-1

            for _ in range(max_retries):
                lag_cand = bisect_int(
                    ac, lower_bound, upper_bound,
                    preferred_positive=False,
                    intercept=tolerance,
                    args=(w, d),
                )

                if lag_cand is None:
                    break
                else:
                    lower_bound = lag_cand
                    lag = lag_cand
                    is_converged = True

            lags[w, d] = lag

    if return_acorr:
        out = lags, acorrs
    else:
        out = lags

    return out


def autocorr_ml(y, thin=1, c=5.0):
    """
    Taken from emcee tutorial on auto correlation.
    https://emcee.readthedocs.io/en/v3.0.2/tutorials/autocorr/
    """
    # Compute the initial estimate of tau using the standard method
    init = autocorr_new(y, c=c)
    z = y[:, ::thin]
    N = z.shape[1]

    # Build the GP model
    tau = max(1.0, init / thin)
    kernel = terms.RealTerm(
        np.log(0.9 * np.var(z)), -np.log(tau), bounds=[(-5.0, 5.0), (-np.log(N), 0.0)]
    )
    kernel += terms.RealTerm(
        np.log(0.1 * np.var(z)),
        -np.log(0.5 * tau),
        bounds=[(-5.0, 5.0), (-np.log(N), 0.0)],
    )
    gp = celerite.GP(kernel, mean=np.mean(z))
    gp.compute(np.arange(z.shape[1]))

    # Define the objective
    def nll(p):
        # Update the GP model
        gp.set_parameter_vector(p)

        # Loop over the chains and compute likelihoods
        v, g = zip(*(gp.grad_log_likelihood(z0, quiet=True) for z0 in z))

        # Combine the datasets
        return -np.sum(v), -np.sum(g, axis=0)

    # Optimize the model
    p0 = gp.get_parameter_vector()
    bounds = gp.get_parameter_bounds()
    soln = minimize(nll, p0, jac=True, bounds=bounds)
    gp.set_parameter_vector(soln.x)

    # Compute the maximum likelihood tau
    a, c = kernel.coefficients[:2]
    tau = thin * 2 * np.sum(a / c) / np.sum(a)
    return tau


def full_acorr_analysis(
        chains: numpy.ndarray,
        lag_max: int=100, tolerance: typing.Optional[float]=None,
    ) -> typing.Tuple[
        numpy.ndarray, numpy.ndarray,
        typing.Optional[typing.List[int]],
    ]:
    n_samples, n_walkers, n_params = chains.shape

    lags = numpy.arange(1,lag_max+1)
    acorrs = numpy.empty((len(lags), n_walkers, n_params), dtype=numpy.float64)

    for i, lag in enumerate(lags):
        acorrs[i] = acorr(chains, lag)

    if tolerance is not None:
        # Compute the lag after which the autocorrelation first goes below the
        # tolerance along all parameters, with each walker considered
        # individually.
        i_tol = numpy.max(numpy.argmax(acorrs < tolerance, axis=0), axis=1)
        lag_tols = lags[i_tol]
    else:
        lag_tols = None

    return lags, acorrs, lag_tols


def plot_acorrs(
        lags: numpy.ndarray, acorrs: numpy.ndarray,
        lag_tols: typing.Optional[typing.List[int]]=None,
        tolerance: typing.Optional[float]=None,
        param_names: typing.Optional[typing.Tuple[str,...]]=None,
    ):
    # Count number of dimensions from acorrs.
    n_lags, n_walkers, n_params = acorrs.shape

    # Substitute default parameter names.
    if param_names is None:
        param_names = tuple(
            "Param #{}".format(i) for i in range(n_params)
        )

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(figsize=(10,10), constrained_layout=True)

    iterables = enumerate(zip(param_names, colors, linestyles))
    for i, (name, color, linestyle) in iterables:
        labeled = False
        for j in range(n_walkers):
            label = None if labeled else name
            ax.plot(
                lags, acorrs[...,j,i],
                color=color, linestyle=linestyle, alpha=0.1,
                label=label,
            )
            labeled = True

    if tolerance is not None:
        ax.axhline(tolerance, color="black", linestyle="dashed")

    if lag_tols is not None:
        for lag_tol, color in zip(lag_tols, colors):
            ax.axvline(lag_tol, color=color, linestyle="dashed", alpha=0.05)

    ax.set_xlabel("lag")
    ax.set_ylabel("autocorrelation")

    ax.legend(loc="best")

    return fig, ax


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing input raw posterior samples.",
    )

    parser.add_argument(
        "plot_file",
        help="File to output plot to.",
    )

    parser.add_argument(
        "--burnin",
        type=int, default=0,
        help="Number of samples at beginning of chain to discard before "
             "calculating autocorrelations (default is 0).",
        )

    parser.add_argument(
        "--tolerance",
        type=float,
        help="Highest autocorrelation to consider as uncorrelated enough. "
             "Zero would be truly uncorrelated, and one would be completely "
             "correlated, so something close to zero is appropriate.",
    )

    parser.add_argument(
        "--lag-max",
        type=int, default=100,
        help="Maximum time lag to consider.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )

    return parser.parse_args(raw_args)




def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import itertools
    colors = itertools.cycle(__colors)
    linestyles = itertools.cycle(__linestyles)

    import h5py
    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    with H5RawPosteriorSamples(args.posteriors, "r") as posteriors:
        pos = posteriors.get_samples(slice(args.burnin,None))
        n_samples, n_walkers, n_params = pos.shape
        param_names = posteriors.variable_names

    lags = numpy.arange(1,args.lag_max+1)
    acorrs = numpy.empty((len(lags), n_walkers, n_params), dtype=numpy.float64)

    for i, lag in enumerate(lags):
        acorrs[i] = acorr(pos, lag)

    fig, ax = plt.subplots()

    iterables = enumerate(zip(param_names, colors, linestyles))
    for i, (name, color, linestyle) in iterables:
        labeled = False
        for j in range(n_walkers):
            label = None if labeled else name
            ax.plot(
                lags, acorrs[...,j,i],
                color=color, linestyle=linestyle, alpha=0.1,
                label=label,
            )
            labeled = True


    if args.tolerance is not None:
        ax.axhline(args.tolerance, color="black", linestyle="dashed")

        # Compute the lag after which the autocorrelation first goes below the
        # tolerance along all parameters, with each walker considered
        # individually.
        i_tol = numpy.max(numpy.argmax(acorrs < args.tolerance, axis=0), axis=1)
        lag_tols = lags[i_tol]
        print("Lags are {lags}.".format(lags=lag_tols))

        for lag_tol in lag_tols:
            ax.axvline(lag_tol, color="black", linestyle="solid", alpha=0.05)


    ax.set_xlabel("lag")
    ax.set_ylabel("autocorrelation")

    ax.legend(loc="best")

    fig.savefig(args.plot_file)
