def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_mass_distribution")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot_intensity_m1")
    subparser.add_argument("output_plot_pdf_m1")
    subparser.add_argument("output_plot_intensity_chi1")
    subparser.add_argument("output_plot_pdf_chi1")
    subparser.add_argument("output_plot_intensity_chieff")
    subparser.add_argument("output_plot_pdf_chieff")

    subparser.add_argument("--n-plot-points", type=int, default=50)
    subparser.add_argument("--n-samples-per-realization", type=int, default=300)

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.add_argument(
        "--n-oom",
        type=int,
        help="Limit plots' y-axes to the specified number of OoM from the "
             "peak.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....coordinate import CoordinateSystem
    from ....credible_regions import CredibleRegions1D
    from ....astro_models.coordinates import (
        m1_source_coord, m2_source_coord,
        chi1_coord, chieff_coord,
    )
    from ....utils.plotting import limit_ax_oom

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Determine maximum mass.
        M_max = post_samples.metadata["M_max"]
        # Get population object.
        population = get_population(M_max)
        # Split into p(m1), p(chi1), and p(chieff)
        population_m1 = population.to_coords(
            CoordinateSystem(m1_source_coord),
            use_transformations=False,
        )
        population_chi1 = population.to_coords(
            CoordinateSystem(chi1_coord),
            use_transformations=True,
        )
        population_chieff = population.to_coords(
            CoordinateSystem(chieff_coord),
            use_transformations=True,
        )

        # Create wrapper functions for the PDF's and intensity functions, since
        # they need to be used with a single input observable, but the
        # population objects expect a list of input observables.
        def m1_intensity(m1, parameters):
            return population_m1.intensity([m1], parameters)
        def m1_pdf(m1, parameters):
            return population_m1.pdf([m1], parameters)
        def chi1_pdf(chi1, parameters):
            chi1_samples, = population_chi1.rvs(
                cli_args.n_samples_per_realization, parameters,
                random_state=random_state,
            )

            obs_shape = chi1.shape
            params_shape = chi1_samples.shape[:-1]
            out_shape = params_shape + obs_shape

            pdf = numpy.empty(out_shape, dtype=numpy.float64)

            for idx in numpy.ndindex(*params_shape):
                chi1_kde = scipy.stats.gaussian_kde(
                    [chi1_samples[idx]],
                    bw_method="scott",
                )

                pdf[idx] = chi1_kde([chi1])

            return pdf
        def chi1_intensity(chi1, parameters):
            rate = parameters["rate"]
            pdf = chi1_pdf(chi1, parameters)
            return (pdf.T * rate.T).T

        def chieff_pdf(chieff, parameters):
            chieff_samples, = population_chieff.rvs(
                cli_args.n_samples_per_realization, parameters,
                random_state=random_state,
            )

            obs_shape = chieff.shape
            params_shape = chieff_samples.shape[:-1]
            out_shape = params_shape + obs_shape

            pdf = numpy.empty(out_shape, dtype=numpy.float64)

            for idx in numpy.ndindex(*params_shape):
                chieff_kde = scipy.stats.gaussian_kde(
                    [chieff_samples[idx]],
                    bw_method="scott",
                )

                pdf[idx] = chieff_kde([chieff])

            return pdf
        def chieff_intensity(chieff, parameters):
            rate = parameters["rate"]
            pdf = chieff_pdf(chieff, parameters)
            return (pdf.T * rate.T).T


        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine m_min and m_max
        m_min_name, m_max_name = "m_min", "m_max"
        if m_min_name in post_samples.constants:
            # Use the constant value.
            m_min = post_samples.constants[m_min_name]
        else:
            # Use the lowest value in the posterior sample set.
            m_min = parameter_samples[m_min_name].min()
        if m_max_name in post_samples.constants:
            # Use the constant value.
            m_max = post_samples.constants[m_max_name]
        else:
            # Use the highest value in the posterior sample set.
            m_max = parameter_samples[m_max_name].max()
        # Determine q_min and q_max
        q_min, q_max = 1e-5, 1.0

        ## Plot m1
        m1_grid = numpy.logspace(
            numpy.log10(m_min), numpy.log10(m_max),
            cli_args.n_plot_points,
        )
        m1_intensity_ci = CredibleRegions1D.from_samples(
            m1_intensity, m1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        m1_intensity_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"$\mathcal{R} \, p(m_{1,\mathrm{source}} / M_\odot)$")

        ax.set_xscale("log")
        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_intensity_m1)
        # Free up memory
        plt.close(fig)
        del m1_intensity_ci

        m1_pdf_ci = CredibleRegions1D.from_samples(
            m1_pdf, m1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        m1_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"$p(m_{1,\mathrm{source}} / M_\odot)$")

        ax.set_xscale("log")
        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_pdf_m1)
        # Free up memory
        plt.close(fig)
        del m1_grid, m1_pdf_ci


        ## Plot chi1
        chi1_grid = numpy.linspace(0.0, 1.0, cli_args.n_plot_points)
        chi1_intensity_ci = CredibleRegions1D.from_samples(
            chi1_intensity, chi1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        chi1_intensity_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$\chi_1$")
        ax.set_ylabel(r"$\mathcal{R} \, p(\chi_1)$")

        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_intensity_chi1)
        # Free up memory
        plt.close(fig)
        del chi1_intensity_ci

        chi1_pdf_ci = CredibleRegions1D.from_samples(
            chi1_pdf, chi1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        chi1_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$\chi_1$")
        ax.set_ylabel(r"$p(\chi_1)$")

        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_pdf_chi1)
        # Free up memory
        plt.close(fig)
        del chi1_grid, chi1_pdf_ci


        ## Plot chieff
        chieff_grid = numpy.linspace(-1.0, +1.0, cli_args.n_plot_points)
        chieff_intensity_ci = CredibleRegions1D.from_samples(
            chieff_intensity, chieff_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        chieff_intensity_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$\chi_{\mathrm{eff}}$")
        ax.set_ylabel(r"$\mathcal{R} \, p(\chi_{\mathrm{eff}})$")

        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_intensity_chieff)
        # Free up memory
        plt.close(fig)
        del chieff_intensity_ci

        chieff_pdf_ci = CredibleRegions1D.from_samples(
            chieff_pdf, chieff_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        chieff_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$\chi_{\mathrm{eff}}$")
        ax.set_ylabel(r"$p(\chi_{\mathrm{eff}})$")

        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_pdf_chieff)
        # Free up memory
        plt.close(fig)
