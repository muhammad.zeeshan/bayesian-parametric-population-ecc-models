def populate_subparser(subparsers):
    subparser = subparsers.add_parser("pearson_correlation")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument(
        "--coordinates",
        nargs="+", default=["m1_source", "chi1"],
        help="Coordinate system for the correlation matrix.",
    )

    subparser.add_argument(
        "--n-samples",
        type=int, default=100000,
        help="Number of samples to use in the Monte Carlo.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output even if file exists.",
    )

    subparser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Matplotlib backend to use for plotting.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....coordinate import CoordinateSystem
    from ....credible_regions import CredibleRegions1D
    from ....astro_models.coordinates import coordinates
    from ....utils.plotting import limit_ax_oom

    import itertools

    import h5py
    import numpy
    import matplotlib
    matplotlib.use(cli_args.mpl_backend)
    import matplotlib.pyplot as plt

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Determine maximum mass.
        M_max = post_samples.metadata["M_max"]
        # Determine coordinate system.
        coord_system = CoordinateSystem(
            *(coordinates[coord_name] for coord_name in cli_args.coordinates)
        )
        # Get population object.
        population = get_population(M_max).to_coords(coord_system)
        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)
        # Compute correlation coefficients
        corr_matrix = population.corrcoef(
            parameter_samples,
            n_samples=cli_args.n_samples,
            random_state=random_state,
        )

        h5_mode = "w" if cli_args.force else "w-"
        with h5py.File(cli_args.output_hdf5, h5_mode) as h5_file:
            h5_file.create_dataset("corr_matrix", data=corr_matrix)

        # Set up figures.
        n_coords = len(coord_system)
        n_rows_cols = n_coords-1
        fig, axes = plt.subplots(
            n_rows_cols, n_rows_cols,
            figsize=(3*n_rows_cols, 3*n_rows_cols),
            sharex="col",
            squeeze=False,
        )

        row_iter = enumerate(range(1, n_coords))
        col_iter = enumerate(range(n_coords-1))
        for (row, y_idx), (col, x_idx) in itertools.product(row_iter, col_iter):
            # Pull out axis
            ax = axes[row,col]

            # Skip anything above diagonal, as it's all ones.  Hide subplot.
            if y_idx <= x_idx:
                ax.axis("off")
                continue

            # Plot histogram of correlations.
            corr_samples = corr_matrix[...,x_idx,y_idx]
            ax.hist(
                corr_samples,
                histtype="step", density=True,
                range=[-1.0, +1.0],
                bins="auto",
                color="C0",
            )

            # Hide y-axis labels, as they're not important.
            ax.set_yticklabels([])

        for row, y_idx in enumerate(range(1, n_coords)):
            axes[row,0].set_ylabel(coord_system[y_idx].name)
        for col, x_idx in enumerate(range(n_coords-1)):
            axes[-1,col].set_xlabel(coord_system[x_idx].name)

        fig.tight_layout()

        fig.savefig(cli_args.output_plot)
