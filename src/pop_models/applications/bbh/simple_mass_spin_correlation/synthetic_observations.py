def populate_subparser(subparsers):
    subparser = subparsers.add_parser("synthetic_observations")

    subparser.add_argument(
        "truths",
        help="JSON file mapping parameter names to their true values.",
    )

    subparser.add_argument(
        "VT",
        help="HDF5 file with VT tabulated.",
    )

    subparser.add_argument(
        "n_detections",
        type=int,
        help="Fixed number of detections.",
    )

    subparser.add_argument(
        "--total-mass-max",
        type=float, default=200.0,
        help="Maximum total mass in Msun.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import json

    import h5py
    import numpy

    from .population import get_population

    from pop_models.poisson_mean import MonteCarloVolumeIntegralPoissonMean
    from pop_models.coordinate import CoordinateSystem
    from pop_models.synthesis import ObservationSynthesizer

    from pop_models.astro_models.building_blocks.powerlaw import powerlaw_rvs
    from pop_models.astro_models.gw_ifo_vt import RegularGridVTInterpolator


    # Get population and coord_system in default EOS coordinate sytem
    population = get_population(cli_args.total_mass_max)

    # Load parameters from input file
    with open(cli_args.truths, "r") as truths_file:
        parameters_true = {
            name : numpy.asarray(value)
            for name, value in json.load(truths_file).items()
        }


    # Seed the RNG
    random_state = numpy.random.RandomState(cli_args.seed)

    # Load VT file.
    with h5py.File(cli_args.VT, "r") as vt_file:
        VT = RegularGridVTInterpolator(vt_file)

    obs_synth = ObservationSynthesizer(
        population,
        detection_count=cli_args.n_detections,
        volume=VT,
    )

    observations = obs_synth.sample(parameters_true, random_state=random_state)

    n_samples = len(observations[0])

    print("#", "\t".join(coord.name for coord in population.coord_system))
    for i in range(n_samples):
        print("\t".join(str(obs[i]) for obs in observations))
