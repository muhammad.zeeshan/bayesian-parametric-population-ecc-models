def populate_subparser(subparsers):
    subparser = subparsers.add_parser("inference")

    subparser.add_argument(
        "events",
        nargs="*",
        help="List of posterior sample files, one for each event.",
    )
    subparser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    subparser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    subparser.add_argument(
        "posterior_output",
        help="HDF5 file to store posterior samples in.",
    )

    subparser.add_argument(
        "--n-powerlaw-parts",
        type=int, default=2,
        help="Number of parts in broken powerlaw (default 2).",
    )

    subparser.add_argument(
        "--constants",
        help="JSON file fixing population parameters to constants.",
    )

    subparser.add_argument(
        "--scale-factor",
        type=float, default=None,
        help="Multiply all VTs by this constant factor.",
    )

    subparser.add_argument(
        "--vt-calibration",
        help="Calibration file for VT's.",
    )

    subparser.add_argument(
        "--n-walkers",
        default=None, type=int,
        help="Number of walkers to use, defaults to twice the number of "
             "dimensions.",
    )
    subparser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of MCMC samples per walker.",
    )
    subparser.add_argument(
        "--n-threads",
        default=1, type=int,
        help="Number of threads to use in MCMC.",
    )
    subparser.add_argument(
        "--chunk-size",
        type=int, default=1024,
        help="Number of samples to store in each posterior sample sub-file.",
    )

    subparser.add_argument(
        "--weights-field",
        help="Name of field in input posteriors that specifies weights to "
             "apply to posterior samples.  Will multiply by these values if "
             "provided.",
    )
    subparser.add_argument(
        "--inv-weights-field",
        help="Name of field in input posteriors that specifies inverse weights "
             "to apply to posterior samples.  Will divide by these values if "
             "provided.",
    )

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output, even if file already exists. Will overwrite.",
    )

    subparser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    subparser.add_argument(
        "--swmr-mode",
        action="store_true",
        help="Open file in single-writer multiple-reader (SWMR) mode, so that "
             "other processes may read from the file while it's running, and "
             "so that it will be more resistant to data corruption.",
    )

    subparser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_param_names, get_population, coord_system
    from .prior import Prior

    from ....detection import ReweightedPosteriorsDetectionLikelihood
    from ....poisson_mean import MonteCarloVolumeIntegralFixedSamplePoissonMean
    from ....posterior import (
        H5RawPosteriorSamples,
        PopulationInferenceEmceeSampler,
    )

    from ....astro_models.gw_ifo_vt import RegularGridVTInterpolator
    from ....astro_models.detections.cbc.pre_sampled import (
        CBCPreSampledDetection
    )

    import h5py
    import json
    import numpy

    import os

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load existing posterior samples if resuming (file exists and --force flag
    # not set).
    exists = os.path.isfile(
        os.path.join(cli_args.posterior_output, "master.hdf5")
    )
    if exists and not cli_args.force:
        resumed = True
        post_samples = H5RawPosteriorSamples(
            cli_args.posterior_output, "r+",
            swmr_mode=cli_args.swmr_mode,
        )

        metadata = post_samples.metadata
        pop_prior_settings = json.loads(metadata["pop_prior_settings"])
        n_powerlaw_parts = metadata["n_powerlaw_parts"]
        param_names = get_param_names(n_powerlaw_parts)

        # Construct prior object from metadata in posterior samples.
        prior = Prior(
            random_state,
            pop_prior_settings,
            n_powerlaw_parts,
            constants=post_samples.constants,
            duplicates=post_samples.duplicates,
        )
    # Create object for storing posterior samples, with an HDF5 backend, if
    # not resuming (file doesn't exist, or --force flag set).
    else:
        resumed = False
        n_powerlaw_parts = cli_args.n_powerlaw_parts
        param_names = get_param_names(n_powerlaw_parts)
        pop_prior_settings, constants, duplicates = parse_consts_and_prior(
            cli_args.pop_priors, cli_args.constants,
            n_powerlaw_parts,
        )

        # Construct prior object.
        prior = Prior(
            random_state,
            pop_prior_settings,
            n_powerlaw_parts,
            constants=constants, duplicates=duplicates,
        )

        # Initialize MCMC
        init_state = prior.init_walkers(cli_args.n_walkers)
        # Create posterior samples HDF5 archive, and object interface
        post_samples = H5RawPosteriorSamples.create(
            cli_args.posterior_output,
            param_names,
            init_state,
            constants=constants, duplicates=duplicates,
            chunk_size=cli_args.chunk_size,
            force=cli_args.force,
            swmr_mode=cli_args.swmr_mode,
        )
        # Store metadata
        post_samples.metadata["pop_prior_settings"] = json.dumps(
            pop_prior_settings,
        )
        post_samples.metadata["n_powerlaw_parts"] = n_powerlaw_parts
        post_samples.metadata["VT_scale_factor"] = (
            cli_args.scale_factor if cli_args.scale_factor is not None
            else 1.0
        )

    # Open tabular files for event posteriors.
    detections = [
        CBCPreSampledDetection.load(
            event_fname,
            coord_system=coord_system,
            weights_field=cli_args.weights_field,
            inv_weights_field=cli_args.inv_weights_field,
        )
        for event_fname in cli_args.events
    ]

    # Get appropriate population object.
    population = get_population(n_powerlaw_parts)

    # Load in VT file.
    with h5py.File(cli_args.VTs, "r") as vt_file:
        VT = RegularGridVTInterpolator(
            vt_file, scale_factor=post_samples.metadata["VT_scale_factor"],
        )

    # Save the VT mode if we're not resuming
    if not resumed:
        post_samples.metadata["VT_mode"] = VT.mode

    # Evaluates expected number of detections for a given population and VT.
    expval = MonteCarloVolumeIntegralFixedSamplePoissonMean(population, VT)

    # Create all of detection*population integral evaluators.
    detection_likelihoods = [
        ReweightedPosteriorsDetectionLikelihood(population, detection)
        for detection in detections
    ]

    pop_inference = PopulationInferenceEmceeSampler(
        post_samples,
        expval, detection_likelihoods, prior.log_prior,
        random_state=random_state,
        verbose=cli_args.verbose,
    )

    pop_inference.posterior_samples(cli_args.n_samples)


def parse_consts_and_prior(
        priors_fname, constants_fname,
        n_powerlaw_parts,
    ):
    import six
    import json
    from .population import get_param_names
    from .... import utils

    param_names = get_param_names(n_powerlaw_parts)

    # Load the population priors from a JSON file.
    with open(priors_fname, "r") as priors_file:
        pop_prior_settings = json.load(priors_file)

        # TODO: validate file structure


    # Load in constants file.
    if constants_fname is None:
        # No constants to be loaded.
        constants = {}
    else:
        # Load constants in from a JSON file.
        with open(constants_fname, "r") as constants_file:
            constants = json.load(constants_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(constants, dict):
            raise TypeError(
                "Constants file must be a dictionary mapping param -> value"
            )
        # Check that the keys are all param names.
        unknown_params = [
            param for param in constants
            if param not in param_names
        ]
        if len(unknown_params) != 0:
            raise ValueError(
                "Unrecognized params in constants file:\n{}"
                .format(utils.format_set_in_quotes(unknown_params))
            )
        # Check that all of the param names map to floats.
        # NOTE: May have to change this if we ever allow for MCMC's over things
        # other than floats (integers?), but currently our underlying sampler
        # only supports floats.
        bad_values = []
        for param, value in six.iteritems(constants):
            if not isinstance(value, float):
                bad_values.append(param)
        if len(bad_values) != 0:
            raise TypeError(
                "The following constants mapped to invalid values:\n{}"
                .format(utils.format_set_in_quotes(bad_values))
            )


    # Parse constants file, and check for any conflicts with priors file.
    conflicting_priors = []
    for param in constants:
        # Check if parameter has already been given a prior. If so, we're going
        # to raise an exception once we've found all such parameters. We could
        # just override those priors, but it's better to be safe.
        if param in pop_prior_settings:
            conflicting_priors.append(param)
        # For bookkeeping purposes, set the parameter's prior distribution to
        # "constant". Map "params" to an empty dict for consistency with other
        # distributions that require params.
        else:
            pop_prior_settings[param] = {
                "dist": "constant",
                "params": {}
            }

    # Raise an exception if any parameters were set to constants, when they were
    # already given priors. List all of the offending params so the user can
    # correct the issue.
    if len(conflicting_priors) != 0:
        raise ValueError(
            "The following parameters were set to constants, but also had "
            "priors set in 'pop_priors' file. Those priors would need to "
            "be ignored, so remove them from the 'pop_priors' file and rerun "
            "if that is desired.\n{}"
            .format(utils.format_set_in_quotes(conflicting_priors))
        )

    # For now, no duplicates.
    duplicates = {}

    # Raise an exception if any params do not have priors specified now.
    missing_params = [
        param for param in param_names
        if param not in pop_prior_settings
    ]
    if len(missing_params) != 0:
        raise ValueError(
            "The following parameters have not been given priors:\n{}"
            .format(utils.format_set_in_quotes(missing_params))
        )

    return pop_prior_settings, constants, duplicates
