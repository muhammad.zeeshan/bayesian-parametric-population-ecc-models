from ...astro_models.building_blocks.powerlaw import (
    ComponentMassPowerlawPopulation,
)
from ...posterior import (
    PopulationInferenceEmceeSampler,
    ReweightedPosteriorsDetectionLikelihood,
    MonteCarloVolumeIntegralPoissonMean,
)
from ...volume import SensitiveVolume

import numpy


rate_min, rate_max = 1e-6, 1e4
m_min_min, m_min_max = 5.0, 50.0
m_max_min, m_max_max = 50.0, 100.0
M_max = 2.0*m_max_max

def log_prior(parameters):
    rate = parameters["rate"]
    m_min = parameters["m_min"]
    m_max = parameters["m_max"]

    cond = (
        (m_min < m_max) &
        (rate_min <= rate) &
        (rate <= rate_max) &
        (m_min_min <= m_min) &
        (m_min <= m_min_max) &
        (m_max_min <= m_max) &
        (m_max <= m_max_max)
    )

    return numpy.where(cond, 0.0, -numpy.inf)


population = ComponentMassPowerlawPopulation(M_max)
