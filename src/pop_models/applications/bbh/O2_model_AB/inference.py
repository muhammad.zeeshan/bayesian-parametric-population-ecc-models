def populate_subparser(subparsers):
    subparser = subparsers.add_parser("inference")

    subparser.add_argument(
        "events",
        nargs="*",
        help="List of posterior sample files, one for each event.",
    )
    subparser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    subparser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    subparser.add_argument(
        "posterior_output",
        help="HDF5 file to store posterior samples in.",
    )

    init_from_file_group = subparser.add_mutually_exclusive_group()
    init_from_file_group.add_argument(
        "--init-from-raw-samples",
        help="Initialize chains from the given HDF5 raw samples archive, "
             "taking the final positions of all the walkers as the new "
             "starting point.",
    )
    init_from_file_group.add_argument(
        "--init-from-cleaned-samples",
        help="Initialize chains from the given HDF5 cleaned samples file, "
             "taking a random set of samples, specified by --n-walkers, as the "
             "starting point.",
    )

    subparser.add_argument(
        "--constants",
        help="JSON file fixing population parameters to constants.",
    )

    subparser.add_argument(
        "--moves",
        help="JSON file setting ensemble MCMC move options.",
    )

    subparser.add_argument(
        "--iid-spins",
        action="store_true",
        help="Assume that spin magnitudes are independent and identically "
             "distributed (iid). This means that the model parameters for chi1 "
             "and chi2 are forced to be the same. This is accomplished in "
             "practice by using the parameter for chi1 anywhere it is needed "
             "for chi2. If any chi2 parameters were specified by the user, an "
             "exception will be raised.",
    )
    subparser.add_argument(
        "--no-spin-singularities",
        action="store_true",
        help="Restrict the spin distribution such that spin distributions with "
             "singularities are not allowed by the prior.",
    )

    subparser.add_argument(
        "--scale-factor",
        type=float, default=None,
        help="Multiply all VTs by this constant factor.",
    )

    subparser.add_argument(
        "--vt-calibration",
        help="Calibration file for VT's.",
    )

    subparser.add_argument(
        "--n-walkers",
        default=None, type=int,
        help="Number of walkers to use, defaults to twice the number of "
             "dimensions.",
    )
    subparser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of MCMC samples per walker.",
    )
    subparser.add_argument(
        "--n-threads",
        default=1, type=int,
        help="Number of threads to use in MCMC.",
    )
    subparser.add_argument(
        "--chunk-size",
        type=int, default=1024,
        help="Number of samples to store in each posterior sample sub-file.",
    )

    subparser.add_argument(
        "--weights-field",
        help="Name of field in input posteriors that specifies weights to "
             "apply to posterior samples.  Will multiply by these values if "
             "provided.",
    )
    subparser.add_argument(
        "--inv-weights-field",
        help="Name of field in input posteriors that specifies inverse weights "
             "to apply to posterior samples.  Will divide by these values if "
             "provided.",
    )

    subparser.add_argument(
        "--total-mass-max",
        type=float,
        help="Maximum total mass allowed.",
    )

    subparser.add_argument(
        "--mc-err-abs",
        type=float, default=1e-5,
        help="Allowed absolute error for Monte Carlo integrator.",
    )
    subparser.add_argument(
        "--mc-err-rel",
        type=float, default=1e-3,
        help="Allowed relative error for Monte Carlo integrator.",
    )

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output, even if file already exists. Will overwrite.",
    )

    subparser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    subparser.add_argument(
        "--swmr-mode",
        action="store_true",
        help="Open file in single-writer multiple-reader (SWMR) mode, so that "
             "other processes may read from the file while it's running, and "
             "so that it will be more resistant to data corruption.",
    )

    subparser.add_argument(
        "--backend",
        default="numpy", choices=["numpy", "numba"],
    )

    subparser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )
    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import pop_models
    pop_models.use(cli_args.backend)

    from .population import get_population, coord_system
    from .prior import Prior

    from pop_models.detection import (
        DeterministicReweightedPosteriorsDetectionLikelihood,
    )
    from pop_models.poisson_mean import (
        MonteCarloVolumeIntegralFixedSamplePoissonMean,
    )
    from pop_models.posterior import (
        H5RawPosteriorSamples,
        PopulationInference, PopulationInferenceEmceeSampler,
    )
    from pop_models.utils import debug_verbose

    from pop_models.astro_models.gw_ifo_vt import RegularGridVTInterpolator
    from pop_models.astro_models.detections.cbc.pre_sampled import (
        CBCPreSampledDetection
    )

    import h5py
    import json
    import numpy

    import os

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load existing posterior samples if resuming (file exists and --force flag
    # not set).
    exists = os.path.isfile(
        os.path.join(cli_args.posterior_output, "master.hdf5")
    )
    if exists and not cli_args.force:
        resumed = True
        post_samples = H5RawPosteriorSamples(
            cli_args.posterior_output, "r+",
            swmr_mode=cli_args.swmr_mode,
        )

        metadata = post_samples.metadata
        pop_prior_settings = json.loads(metadata["pop_prior_settings"])
        M_max = metadata["M_max"]
        no_spin_singularities = metadata["no_spin_singularities"]
        iid_spins = metadata["iid_spins"]
        VT_scale_factor = metadata["VT_scale_factor"]

        # Get appropriate population object.
        population = get_population(M_max)

        # Construct prior object from metadata in posterior samples.
        prior = Prior(
            random_state, population.param_names,
            pop_prior_settings,
            M_max,
            no_spin_singularities=no_spin_singularities,
            constants=post_samples.constants,
            duplicates=post_samples.duplicates,
        )
    # Create object for storing posterior samples, with an HDF5 backend, if
    # not resuming (file doesn't exist, or --force flag set).
    else:
        resumed = False

        # Get appropriate population object.
        population = get_population(cli_args.total_mass_max)

        M_max = cli_args.total_mass_max
        no_spin_singularities = cli_args.no_spin_singularities
        iid_spins = cli_args.iid_spins
        VT_scale_factor = (
            cli_args.scale_factor if cli_args.scale_factor is not None
            else 1.0
        )

        pop_prior_settings, constants, duplicates = parse_consts_and_prior(
            cli_args.pop_priors, cli_args.constants, population.param_names,
            iid_spins=iid_spins,
        )

        # Construct prior object.
        prior = Prior(
            random_state, population.param_names,
            pop_prior_settings,
            M_max,
            no_spin_singularities=no_spin_singularities,
            constants=constants, duplicates=duplicates,
        )


    # Open tabular files for event posteriors.
    detections = [
        CBCPreSampledDetection.load(
            event_fname,
            coord_system=coord_system,
            weights_field=cli_args.weights_field,
            inv_weights_field=cli_args.inv_weights_field,
        )
        for event_fname in cli_args.events
    ]

    # Load in VT file.
    with h5py.File(cli_args.VTs, "r") as vt_file:
        VT = RegularGridVTInterpolator(vt_file, scale_factor=VT_scale_factor)

    # Evaluates expected number of detections for a given population and VT.
    expval = MonteCarloVolumeIntegralFixedSamplePoissonMean(population, VT)

    # Create all of detection*population integral evaluators.
    detection_likelihoods = [
        DeterministicReweightedPosteriorsDetectionLikelihood(
            population, detection,
        )
        for detection in detections
    ]

    if not resumed:
        # Create posterior samples HDF5 archive, and object interface.
        if cli_args.init_from_raw_samples is not None:
            # Initialize from raw samples file.
            init_post = H5RawPosteriorSamples(
                cli_args.init_from_raw_samples, "r",
            )
            with init_post:
                post_samples = H5RawPosteriorSamples.create_from_raw_samples(
                    cli_args.posterior_output,
                    init_post,
                    constants=constants,
                    chunk_size=cli_args.chunk_size,
                    force=cli_args.force,
                    swmr_mode=cli_args.swmr_mode,
                )
        elif cli_args.init_from_cleaned_samples is not None:
            # Initialize from cleaned samples file.
            init_post = H5CleanedPosteriorSamples(
                cli_args.init_from_cleaned_samples
            )
            with init_post:
                post_samples = H5RawPosteriorSamples.create_from_samples(
                    cli_args.posterior_output,
                    cli_args.n_walkers,
                    init_post,
                    constants=constants,
                    chunk_size=cli_args.chunk_size,
                    force=cli_args.force,
                    swmr_mode=cli_args.swmr_mode,
                )
        else:
            # Set up a stripped down inference object only used for vetoing
            # initial values that are not within the posterior's support.
            pop_inference_for_init = PopulationInference(
                expval, detection_likelihoods, prior.log_prior,
                population.param_names,
            )

            # Initialize from prior, vetoing based on posterior.
            init_state = prior.init_walkers(
                cli_args.n_walkers, pop_inf=pop_inference_for_init,
            )
            post_samples = H5RawPosteriorSamples.create(
                cli_args.posterior_output,
                population.param_names,
                init_state,
                constants=constants, duplicates=duplicates,
                chunk_size=cli_args.chunk_size,
                force=cli_args.force,
                swmr_mode=cli_args.swmr_mode,
            )

        # Store metadata
        if cli_args.total_mass_max is not None:
            post_samples.metadata["M_max"] = M_max
        post_samples.metadata["no_spin_singularities"] = no_spin_singularities
        post_samples.metadata["pop_prior_settings"] = json.dumps(
            pop_prior_settings,
        )
        post_samples.metadata["iid_spins"] = iid_spins
        post_samples.metadata["VT_scale_factor"] = VT_scale_factor
        post_samples.metadata["VT_mode"] = VT.mode

    with post_samples:
        if cli_args.moves is None:
            moves = None
        else:
            debug_verbose(
                "Parsing MCMC move options.",
                mode="inference_setup", flush=True,
            )
            with open(cli_args.moves, "r") as moves_file:
                moves = PopulationInferenceEmceeSampler.parse_moves_from_file(
                    moves_file
                )
            debug_verbose(
                "MCMC move options parsed as:", moves,
                mode="inference_setup", flush=True,
            )

        debug_verbose(
            "Constructing posterior sampler.",
            mode="inference_setup", flush=True,
        )
        pop_inference = PopulationInferenceEmceeSampler(
            post_samples,
            expval, detection_likelihoods, prior.log_prior,
            moves=moves,
            random_state=random_state,
            verbose=cli_args.verbose,
        )
        debug_verbose(
            "Posterior sampler constructed: {}".format(pop_inference),
            mode="inference_setup", flush=True,
        )

        debug_verbose(
            "Beginning to sample {} posteriors.".format(cli_args.n_samples),
            mode="inference_setup", flush=True,
        )
        pop_inference.posterior_samples(cli_args.n_samples)
        debug_verbose(
            "Finished sampling {} posteriors.".format(cli_args.n_samples),
            mode="inference_setup", flush=True,
        )


def parse_consts_and_prior(
        priors_fname, constants_fname, param_names,
        iid_spins=False,
    ):
    import six
    import json
    from pop_models import utils

    # Load the population priors from a JSON file.
    with open(priors_fname, "r") as priors_file:
        pop_prior_settings = json.load(priors_file)

        # TODO: validate file structure


    # Load in constants file.
    if constants_fname is None:
        # No constants to be loaded.
        constants = {}
    else:
        # Load constants in from a JSON file.
        with open(constants_fname, "r") as constants_file:
            constants = json.load(constants_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(constants, dict):
            raise TypeError(
                "Constants file must be a dictionary mapping param -> value"
            )
        # Check that the keys are all param names.
        unknown_params = [
            param for param in constants
            if param not in param_names
        ]
        if len(unknown_params) != 0:
            raise ValueError(
                "Unrecognized params in constants file:\n{}"
                .format(utils.format_set_in_quotes(unknown_params))
            )
        # Check that all of the param names map to floats.
        # NOTE: May have to change this if we ever allow for MCMC's over things
        # other than floats (integers?), but currently our underlying sampler
        # only supports floats.
        bad_values = []
        for param, value in six.iteritems(constants):
            if not isinstance(value, float):
                bad_values.append(param)
        if len(bad_values) != 0:
            raise TypeError(
                "The following constants mapped to invalid values:\n{}"
                .format(utils.format_set_in_quotes(bad_values))
            )


    # Parse constants file, and check for any conflicts with priors file.
    conflicting_priors = []
    for param in constants:
        # Check if parameter has already been given a prior. If so, we're going
        # to raise an exception once we've found all such parameters. We could
        # just override those priors, but it's better to be safe.
        if param in pop_prior_settings:
            conflicting_priors.append(param)
        # For bookkeeping purposes, set the parameter's prior distribution to
        # "constant". Map "params" to an empty dict for consistency with other
        # distributions that require params.
        else:
            pop_prior_settings[param] = {
                "dist": "constant",
                "params": {}
            }

    # Raise an exception if any parameters were set to constants, when they were
    # already given priors. List all of the offending params so the user can
    # correct the issue.
    if len(conflicting_priors) != 0:
        raise ValueError(
            "The following parameters were set to constants, but also had "
            "priors set in 'pop_priors' file. Those priors would need to "
            "be ignored, so remove them from the 'pop_priors' file and rerun "
            "if that is desired.\n{}"
            .format(utils.format_set_in_quotes(conflicting_priors))
        )


    # If the spins are assumed to be i.i.d., then we force all of the chi2
    # parameters to assume the values of the associated chi1 parameters.
    if iid_spins:
        duplicates = {
            "E_chi2": "E_chi1",
            "Var_chi2": "Var_chi1",
            "mu_cos2": "mu_cos1",
            "sigma_cos2": "sigma_cos1",
        }
        conflicting_priors = []
        conflicting_consts = []
        for param in duplicates:
            # Check if i.i.d. param has already been given a prior or constant
            # value. If so, we're going to raise an exception once we've found
            # all such parameters, and tabulate them separately so users know
            # whether to check the priors or constants files. We could just
            # override those priors or constants, but it's better to be safe.
            if param in constants:
                conflicting_consts.append(param)
            elif param in pop_prior_settings:
                conflicting_priors.append(param)
            # For bookkeeping purposes, set the parameter's prior distribution
            # to "duplicate". Map "params" to an empty dict for consistency with
            # other distributions that require params.
            else:
                pop_prior_settings[param] = {
                    "dist": "duplicate",
                    "params": {},
                }

        # Raise an exception if any parameters were set to i.i.d., when they
        # were already given priors or constant values. List all of the
        # offending params so the user can correct the issue.
        if len(conflicting_priors) != 0 or len(conflicting_consts) != 0:
            raise ValueError(
                "The following params had priors and/or constants specified "
                "by the user, and would be overridden by --iid-spins. Please "
                "remove them from the priors and/or constants files.\n"
                "Priors:\n"
                "  {}\n"
                "Constants:\n"
                "  {}"
                .format(
                    utils.format_set_in_quotes(conflicting_priors),
                    utils.format_set_in_quotes(conflicting_consts),
                )
            )
    else:
        # No duplicates to be set.
        duplicates = {}


    # Raise an exception if any params do not have priors specified now.
    missing_params = [
        param for param in param_names
        if param not in pop_prior_settings
    ]
    if len(missing_params) != 0:
        raise ValueError(
            "The following parameters have not been given priors:\n{}"
            .format(utils.format_set_in_quotes(missing_params))
        )

    return pop_prior_settings, constants, duplicates
