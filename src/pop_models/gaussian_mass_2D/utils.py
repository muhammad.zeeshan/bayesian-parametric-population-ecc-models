def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()


def parse_special(metadata, param_names):
    if metadata["iid_spins"]:
        duplicates = {
            "log_alpha_chi2": "log_alpha_chi1",
            "log_beta_chi2": "log_beta_chi1",
            "log_sigma_chi2": "log_sigma_chi1",
        }
    else:
        duplicates = {}

    constants = {
        name: metadata[name]
        for name in param_names
        if name in metadata
    }

    return constants, duplicates


def get_params(variables, constants, duplicates, names):
    import six

    if variables.shape[-1] + len(constants) + len(duplicates) != len(names):
        raise ValueError(
            "Incorrect number of variables and constants. "
            "Expected {expected}, but got {actual}."
            .format(
                expected=len(names),
                actual=len(variables)+len(constants)+len(duplicates),
            )
        )

    params = []
    dup_indices = {}
    i = 0

    for name in names:
        if name in constants:
            param = constants[name]
        elif name in duplicates:
            param = None
        else:
            param = variables[...,i]
            i += 1

        params.append(param)

    for target, source in six.iteritems(duplicates):
        params[names.index(target)] = params[names.index(source)]

    return params

    



def loop_constants(iters_and_consts):
    import numpy
    import itertools

    if not isinstance(iters_and_consts, tuple):
        raise TypeError("`iters_and_consts` must be a tuple")

    def loop_if_constant(ic):
        return itertools.repeat(ic) if numpy.isscalar(ic) else ic

    return [
        loop_if_constant(ic)
        for ic in iters_and_consts
    ]


def upcast_scalars(arrays):
    import numpy
    dims = [numpy.ndim(arr) for arr in arrays]
    shape = numpy.shape(arrays[numpy.argmax(dims)])

    if shape == ():
        shape = (1,)

    result = []
    for arr in arrays:
        if numpy.shape(arr) == shape:
            result.append(numpy.asarray(arr))
        else:
            result.append(numpy.tile(arr, shape))

    return result


def limited_oom_range(ys, n_oom):
    """
    Returns the plotting range to use for (a list of) array(s) of y-values
    ``ys``, limiting it to a number of orders of magnitude ``n_oom`` from the
    peak value.
    """
    import numpy

    # Transform to log-scale
    log_ys = numpy.log10(ys)

    # Mask any nan's and inf's that will mess this up.
    log_ys = numpy.ma.masked_invalid(log_ys)

    # Determine extrema in log-scale and round to integers
    log_y_min = numpy.floor(numpy.min(log_ys))
    log_y_max = numpy.ceil(numpy.max(log_ys))

    # Choose appropriately limited minimum value
    log_y_min = max(log_y_min, log_y_max - n_oom)

    return 10**log_y_min, 10**log_y_max
