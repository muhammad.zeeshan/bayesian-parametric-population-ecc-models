import numpy

import typing

from .types import Observables, Parameters
from .coordinate import CoordinateSystem
from .population import Population
from .poisson_mean import PoissonMean
from .volume import SensitiveVolume

class ObservationSynthesizer(object):
    """
    Tool for drawing synthetic observables from a :class:`population.Population`
    object, when the true source parameters are known.  This can be done either
    with or without selection bias, depending on whether or not the optional
    ``volume`` attribute is set.  If the ``volume`` attribute is set, then one
    has the option of determining the number of random draws either by setting
    a fixed number via the ``detection_count`` attribute, or in a
    parameter-dependent manner by setting the ``poisson_mean`` attribute.
    """
    def __init__(
            self,
            population: Population,
            detection_count: typing.Optional[int]=None,
            poisson_mean: typing.Optional[PoissonMean]=None,
            volume: typing.Optional[SensitiveVolume]=None,
            coord_system: typing.Optional[CoordinateSystem]=None,
        ) -> None:
        # Precisely one of these two arguments should be `None`, so the equality
        # will hold when violated.
        if (detection_count is None) == (poisson_mean is None):
            raise ValueError(
                "Must provide precisely one of `detection_count` and "
                "`poisson_mean`"
            )

        # If `poisson_mean` is provided, then it only makes sense that `volume`
        # be provided as well.
        if (poisson_mean is not None) and (volume is None):
            raise ValueError(
                "If `poisson_mean` is provided, `volume` must be as well."
            )

        self.population = population
        self.detection_count = detection_count
        self.poisson_mean = poisson_mean
        self.volume = volume
        self.coord_system = (
            population.coord_system if coord_system is None else coord_system
        )

    def sample(
            self,
            parameters: Parameters,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        """
        Draw random samples from the population specified by ``parameters``.
        Note that this should be a single set of parameters, as the number of
        detections may vary for different parameters.
        """
        if random_state is None:
            random_state = numpy.random.RandomState()

        # Check the shape of the parameters.
        param_shape = numpy.shape(next(iter(parameters.values())))

        if param_shape not in {(), (1,)}:
            raise ValueError(
                "'parameters' must be 1-D, got {}".format(param_shape)
            )

        # Determine how many samples to draw, either via the hard-coded
        # ``self.detection_count``, or in a parameter-dependent fashion with
        # ``self.poisson_mean``.
        if self.detection_count is None:
            mean_n_samples = typing.cast(
                PoissonMean, self.poisson_mean,
            )(parameters)
            n_samples = random_state.poisson(mean_n_samples)
        else:
            n_samples = self.detection_count

        # If a ``volume`` object was not set, then we simply draw the desired
        # number of detections from the population.  Otherwise we need to
        # rejection-sample.
        if self.volume is None:
            return self.population.rvs(
                n_samples, parameters,
                random_state=random_state,
            )

        # Will store generated observables in this tuple of arrays.
        observables = tuple(
            numpy.empty(n_samples) for _ in range(len(self.coord_system))
        )

        # Maximum value ``volume`` can produce.  Needed for rejection sampling.
        V_max = self.volume.max()

        # Transform samples from population's coordinate system to volume's
        # coordinate system, and desired output coordinate system.
        transf_pop_to_vol = self.population.transformations[
            self.population.coord_system, self.volume.coord_system
        ]
        transf_pop_to_out = self.population.transformations[
            self.population.coord_system, self.coord_system
        ]

        # Rejection-sample observations until we have ``n_samples`` of them.
        n_generated = 0
        while n_generated < n_samples:
            # Draw one observation
            obs = self.population.rvs(1, parameters, random_state=random_state)
            # Compute VT(obs) / max(VT), which should work as a relative
            # detection probability.
            p_det = self.volume(transf_pop_to_vol(obs)) / V_max

            # Keep sample with probability ``p_det``
            if p_det > random_state.uniform():
                # Transform observation to desired coordinates.
                obs_out = transf_pop_to_out(obs)
                # Store observation in output
                for dest, data in zip(observables, obs_out):
                    dest[n_generated] = data
                # Record that we've successfully generated one more data point.
                n_generated += 1

        return observables
