import typing

import numpy

from ...coordinate import CoordinateSystem, CoordinateTransforms
from ...population import Population
from ...types import WhereType, Numeric, Observables, Parameters

from ..coordinates import Z_metal_coord, transformations


coord_system = CoordinateSystem(Z_metal_coord)


class MetallicityDistribution(Population):
    def __init__(
            self,
            param_names: typing.Tuple[str,...],
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )


class ConstantMetallicity(MetallicityDistribution):
    def __init__(
            self,
            transformations: CoordinateTransforms=transformations,
            Z_metal_const_name="Z_metal_const",
        ) -> None:
        param_names = (Z_metal_const_name,)
        super().__init__(param_names, transformations=transformations)
        self.Z_metal_const_name = Z_metal_const_name

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
            **kwargs
        ) -> Observables:
        params_shape = numpy.shape(next(iter(parameters.values())))
        out_shape = params_shape + (n_samples,)

        Z_metal = numpy.broadcast_to(
            parameters[self.Z_metal_const_name][...,None],
            out_shape,
        )

        return Z_metal,
