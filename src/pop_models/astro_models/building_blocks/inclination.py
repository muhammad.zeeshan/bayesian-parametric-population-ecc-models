import typing

import math
import numpy

from ...coordinate import CoordinateSystem
from ...population import Population
from ...types import Parameters, Observables, WhereType
from ..coordinates import incl_coord, transformations

coord_system = CoordinateSystem(incl_coord)

class IsotropicInclinationPopulation(Population):
    def __init__(self) -> None:
        param_names = ()
        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        if random_state is None:
            random_state = numpy.random.RandomState()

        incl_samples = numpy.arccos(
            random_state.uniform(0.0, 1.0, n_samples)
        )

        return incl_samples,
