import types
import typing

import numpy

from ...coordinate import (
    Coordinate, CoordinateSystem,
    CoordinateTransforms, transformations_nil,
)
from ...population import Population
from ...types import Numeric, WhereType, Observables, Parameters


def powerlaw_pdf(
        x,
        powers, x_mins, x_maxs,
        where=True,
        xpy=numpy,
    ):
    # Broadcast all parameters to compatible shape.
    powers, x_mins, x_maxs, where = xpy.broadcast_arrays(
        powers, x_mins, x_maxs, where,
    )

    # Compute needed shapes
    obs_shape = x.shape
    param_shape = powers.shape
    out_shape = param_shape + obs_shape

    # Initialize output PDF array.
    pdf = xpy.zeros(out_shape, dtype=x.dtype)
    # Initialize boolean array to determine indices to operate on.
    idx = xpy.zeros(param_shape, dtype=bool)
    where_T = xpy.broadcast_to(where.T, reversed(out_shape))

    # Compute normalization constant in special case when `powers == -1`,
    # which is ``ln(x_mins) - ln(x_maxs)``
    xpy.equal(powers, -1.0, where=where, out=idx)
    xpy.subtract(
        xpy.log(x_mins, where=where), xpy.log(x_maxs, where=where),
        where=idx, out=pdf.T,
    )

    # Compute normalization constant in non-special cases, which is
    # ``(x_max**(1+powers) - x_min**(1+powers)) / (1+powers)``
    xpy.invert(idx, where=where, out=idx)
    one_plus_powers = xpy.add(powers, 1.0, where=idx)
    xpy.divide(
        one_plus_powers,
        xpy.subtract(
            xpy.power(x_maxs, one_plus_powers, where=idx),
            xpy.power(x_mins, one_plus_powers, where=idx),
            where=idx,
        ),
        where=idx,
        out=pdf.T,
    )

    # Multiply on the powerlaw term, `x**powers`, working in the
    # transpose-space.
    xpy.multiply(
        pdf.T,
        xpy.power.outer(x.T, powers.T, where=where_T),
        where=where_T,
        out=pdf.T,
    )

    # Determine where there is PDF support, and return the value of the `pdf`
    # array anywhere it does, and `0.0` otherwise.
    # TODO: Should be able to build this into our indexing earlier.
    support = (
        xpy.less_equal.outer(x_mins, x) & xpy.greater_equal.outer(x_maxs, x)
    )
    return xpy.where(support, pdf, 0.0)


## TODO: Fails for `powers == -1.0` exactly.  Possibly need to handle that case
## specially.  Issue comes with exponentiation to (1 / (1+powers)), which
## results in division by zero, making the returned value 1**inf = 1, so all
## samples are 1.0.
def powerlaw_rvs(
        samples_shape,
        powers, x_mins, x_maxs,
        random_state=None,
        xpy=numpy,
    ):
    # Get a RandomState object if we don't have one already.
    if random_state is None:
        random_state = xpy.random.RandomState()

    # Ensure the `samples_shape` input is a tuple.
    if numpy.ndim(samples_shape) == 0:
        samples_shape = (samples_shape,)
    else:
        samples_shape = tuple(samples_shape)

    # Broadcast all parameters to compatible shape.
    powers, x_mins, x_maxs = xpy.broadcast_arrays(powers, x_mins, x_maxs)

    # Determine shape of various parts of the computation.
    # Output needs to have shape `params_shape + samples_shape`, though for
    # broadcasting reasons we do most of the computation in the transpose-space.
    params_shape = powers.shape
    output_shape = params_shape + samples_shape
    output_shape_T = output_shape[::-1]

    one_plus_powers_T = 1.0 + powers.T

    U = random_state.uniform(size=output_shape_T)

    L = x_mins.T ** one_plus_powers_T
    H = x_maxs.T ** one_plus_powers_T

    return (((1.0-U)*L + U*H) ** (1.0 / one_plus_powers_T)).T



class GenericPowerlawPopulation(Population):
    def __init__(
            self,
            coordinate: Coordinate,
            norm_name: str="norm",
            index_name: str="alpha",
            lower_name: str="x_min", upper_name: str="x_max",
            negative_index: bool=False,
            transformations: CoordinateTransforms=transformations_nil,
            xpy: types.ModuleType=numpy,
        ):
        param_names = (norm_name, index_name, lower_name, upper_name)

        super().__init__(
            CoordinateSystem(coordinate), param_names,
            transformations=transformations,
        )

        self.norm_name = norm_name
        self.index_name = index_name
        self.lower_name = lower_name
        self.upper_name = upper_name

        self.negative_index = negative_index

        self.xpy = numpy

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        x, = observables

        index = parameters[self.index_name]
        if self.negative_index:
            index = -index

        lower = parameters[self.lower_name]
        upper = parameters[self.upper_name]

        return powerlaw_pdf(
            x,
            index, lower, upper,
            where=where,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        index = parameters[self.index_name]
        if self.negative_index:
            index = -index

        lower = parameters[self.lower_name]
        upper = parameters[self.upper_name]

        x_samples = powerlaw_rvs(
            n_samples,
            index, lower, upper,
            random_state=random_state,
            xpy=self.xpy,
        )

        return x_samples,

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.norm_name]


class GenericUniformPopulation(Population):
    def __init__(
            self,
            coordinate: Coordinate,
            norm_name: str="norm",
            lower_name: str="x_min", upper_name: str="x_max",
            transformations: CoordinateTransforms=transformations_nil,
            xpy: types.ModuleType=numpy,
        ):
        param_names = (norm_name, lower_name, upper_name)

        super().__init__(
            CoordinateSystem(coordinate), param_names,
            transformations=transformations,
        )

        self.norm_name = norm_name
        self.lower_name = lower_name
        self.upper_name = upper_name

        self.xpy = numpy

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        x, = observables

        lower = parameters[self.lower_name]
        upper = parameters[self.upper_name]

        shape = lower.shape + x.shape
        shape_T = reversed(shape)

        p_support = numpy.broadcast_to(
            numpy.reciprocal(upper - lower).T,
            shape_T,
        ).T

        i_support = (
            self.xpy.less_equal.outer(lower, x) &
            self.xpy.greater_equal.outer(upper, x)
        )

        return self.xpy.where(i_support, p_support, 0.0)

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        lower = parameters[self.lower_name]
        upper = parameters[self.upper_name]

        size_T = (n_samples,) + self.xpy.shape(self.xpy.transpose(lower))

        x_samples_T = random_state.uniform(
            low=self.xpy.transpose(lower), high=self.xpy.transpose(upper),
            size=size_T
        )

        return x_samples_T.T,

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.norm_name]


_bound_names_type = typing.Tuple[typing.Tuple[str,str]]
class GenericNDUniformPopulation(Population):
    """
    TODO

    NOTE: If ``bound_names`` is None, defaults to names
    ``(x0_min, x0_max, ..., xN_min, xN_max)``
    """
    def __init__(
            self,
            coord_system: CoordinateSystem,
            norm_name: str="norm",
            bound_names: typing.Optional[_bound_names_type]=None,
            transformations: CoordinateTransforms=transformations_nil,
            xpy: types.ModuleType=numpy,
        ):
        if bound_names is None:
            bound_names = tuple(
                ("x{}_min".format(i), "x{}_max".format(i))
                for i in range(len(coord_system))
            )
        elif len(bound_names) != len(coord_system):
            raise ValueError(
                "Number of bound name pairs ({}) does not match "
                "number of coordinates ({})."
                .format(len(bound_names), len(coord_system))
            )
        elif any(len(pair) != 2 for pair in bound_names):
            raise ValueError("Some of the bound name pairs are not pairs.")


        # Flatten bound-names when turning into parameter names.
        param_names = (norm_name,) + sum(bound_names, ())

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

        self.norm_name = norm_name
        self.bound_names = bound_names

        self.xpy = numpy

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Compute the contribution and whether/not it's in the PDF's support for
        # the first dimension.
        x0 = observables[0]
        x0_min = parameters[self.bound_names[0][0]]
        x0_max = parameters[self.bound_names[0][1]]

        p_inv_support = self.xpy.subtract(x0_max, x0_min)

        i_support = (
            self.xpy.less_equal.outer(x0_min, x0) &
            self.xpy.greater_equal.outer(x0_max, x0)
        )

        # Compute the contribution and whether/not it's in the PDF's support for
        # each of the remaining dimensions.
        for i in range(1, len(self.coord_system)):
            xi = observables[i]
            xi_min = parameters[self.bound_names[i][0]]
            xi_max = parameters[self.bound_names[i][1]]

            p_inv_support *= self.xpy.subtract(xi_max, xi_min)

            i_support &= self.xpy.less_equal.outer(xi_min, xi)
            i_support &= self.xpy.greater_equal.outer(xi_max, xi)

        p_support = self.xpy.reciprocal(p_inv_support, out=p_inv_support)
        return self.xpy.where(i_support[...,numpy.newaxis], p_support, 0.0)

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        x_samples_list = []
        # Compute the contributed samples along each dimension.
        for xi_min_name, xi_max_name in self.bound_names:
            xi_min = parameters[xi_min_name]
            xi_max = parameters[xi_max_name]

            size_T = (n_samples,) + self.xpy.shape(self.xpy.transpose(xi_min))

            xi_samples = random_state.uniform(
                low=self.xpy.transpose(xi_min), high=self.xpy.transpose(xi_max),
                size=size_T
            ).T

            x_samples_list.append(xi_samples)

        return tuple(x_samples_list)

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.norm_name]
