import math
import numpy

import typing

from ..types import Observables, Parameters
from ..coordinate import CoordinateSystem
from ..population import Population
from ..poisson_mean import PoissonMean
from ..volume import SensitiveVolume

from .coordinates import (
    m1_source_coord, m2_source_coord,
    chi1_coord, costilt1_coord, chi2_coord, costilt2_coord,
    z_coord, incl_coord, ra_coord, dec_coord,
)

import astropy.cosmology as cosmo
import astropy.units as units

class CBCDetectionCriterion(object):
    def __init__(
            self, coord_system: CoordinateSystem,
        ) -> None:
        self.coord_system = coord_system

    def detected(
            self,
            observables: Observables,
        ) -> typing.Tuple[bool,float]:
        """
        Returns whether or not an object with the given observable is
        detectable, as well as the detection statistic used to determine this.
        """
        raise NotImplementedError()


class SingleIFOSNRCBCDetectionCriterion(CBCDetectionCriterion):
    def __init__(
            self,
            psd_fn, approximant,
            snr_thresh: float=8.0,
            fmin: float=19.0, fmax: float=2048.0, fref: float=40.0,
            psdstart: float=20.0,
            cosmology: cosmo.core.Cosmology=cosmo.Planck15,
            random_state: numpy.random.RandomState=numpy.random.RandomState(),
        ):
        super().__init__(
            CoordinateSystem(
                m1_source_coord, m2_source_coord,
                chi1_coord, costilt1_coord, chi2_coord, costilt2_coord,
                z_coord, incl_coord, ra_coord, dec_coord,
            )
        )
        self.psd_fn = psd_fn
        self.approximant = approximant

        self.snr_thresh = snr_thresh

        self.fmin = fmin
        self.fmax = fmax
        self.fref = fref
        self.psdstart = psdstart

        self.cosmology = cosmology

        self.random_state = random_state

    def detected(
            self,
            observables: Observables,
        ) -> bool:
        import lal
        import lalsimulation as ls

        # Extract observables, ensuring they're all python scalars.
        (
            m1_source, m2_source,
            chi1, costilt1, chi2, costilt2,
            z, incl, ra, dec,
        ) = (
            numpy.asarray(obs).item()
            for obs in observables
        )

        # Compute cartesian components of spin, assuming azimuthal angle is
        # zero.
        chi1x = chi1 * math.sin(math.acos(costilt1))
        chi2x = chi2 * math.sin(math.acos(costilt2))
        chi1y = 0.0
        chi2y = 0.0
        chi1z = chi1 * costilt1
        chi2z = chi2 * costilt2

        # Convert source-frame masses to detector-frame masses, in SI units.
        m1_det_SI = (1+z)*m1_source * lal.MSUN_SI
        m2_det_SI = (1+z)*m2_source * lal.MSUN_SI
        # Use cosmology to convert redshift to luminosity distance in SI units.
        dL_SI = self.cosmology.luminosity_distance(z).to(units.meter).value

        # Conservative estimate of chirp time + merger-ringdown time (2 seconds)
        tmax = ls.SimInspiralChirpTimeBound(
            self.fmin,
            m1_det_SI, m2_det_SI,
            chi1z, chi2z,
        ) + 2.0

        # Convert into frequency-step.
        df = 1.0 / next_pow_two(tmax)

        # Compute h_+(f) and h_x(f)
        hp, hc = ls.SimInspiralChooseFDWaveform(
            m1_det_SI, m2_det_SI,
            chi1x, chi1y, chi1z, chi2x, chi2y, chi2z,
            dL_SI,
            incl, 0.0, 0.0, 0.0, 0.0,
            df, self.fmin, self.fmax, self.fref, None, self.approximant,
        )

        # TODO: get correct h(f) using antenna pattern
        h_of_f = hp

        Nf = int(numpy.round(self.fmax / df)) + 1
        fs = numpy.linspace(0, self.fmax, Nf)
        sel = fs > self.psdstart

        # PSD
        sffs = lal.CreateREAL8FrequencySeries(
            "psds", 0, 0.0, df, lal.DimensionlessUnit, fs.shape[0],
        )
        self.psd_fn(sffs, self.psdstart)

        # Compute the SNR
        snr = ls.MeasureSNRFD(h_of_f, sffs, self.psdstart, -1.0)

        # Return whether/not the SNR was above the cut, as well as the SNR value
        # itself.
        return snr >= self.snr_thresh, snr


class CBCObservationSynthesizer(object):
    """
    """
    def __init__(
            self,
            population: Population,
            detection_count: int,
            detection_criterion: CBCDetectionCriterion,
            coord_system: typing.Optional[CoordinateSystem]=None,
            verbose: bool=False,
        ) -> None:
        self.population = population
        self.detection_count = detection_count
        self.detection_criterion = detection_criterion
        self.coord_system = (
            population.coord_system if coord_system is None else coord_system
        )

        self.transf_to_det_criterion = self.population.transformations[
            self.population.coord_system, self.detection_criterion.coord_system
        ]
        self.transf_to_obs = self.population.transformations[
            self.population.coord_system, self.coord_system
        ]

        self.verbose = verbose

    def sample(
            self,
            parameters: Parameters,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> typing.Tuple[Observables,numpy.ndarray]:
        """
        Draw random samples from the population specified by ``parameters``.
        Note that this should be a single set of parameters, as the number of
        detections may vary for different parameters.  Will return a set of
        observed parameters, and an array corresponding to their detection
        statistics.
        """
        import sys

        if random_state is None:
            random_state = numpy.random.RandomState()

        # Check the shape of the parameters.
        param_shape = numpy.shape(next(iter(parameters.values())))

        if param_shape not in {(), (1,)}:
            raise ValueError(
                "'parameters' must be 1-D, got {}".format(param_shape)
            )

        # Will store generated observables in this list of arrays, and the
        # detection statistics in another array.
        observables = [
            numpy.empty(self.detection_count)
            for _ in range(len(self.coord_system))
        ]
        detection_stats = numpy.empty(self.detection_count)

        # Rejection-sample observations until we have ``self.detection_count``
        # of them.
        n_generated = 0
        while n_generated < self.detection_count:
            # Draw one observation
            obs = self.population.rvs(1, parameters, random_state=random_state)

            # Compute the detection statistic, and determine if it passes
            is_detected, statistic = self.detection_criterion.detected(
                self.transf_to_det_criterion(obs)
            )
            if self.verbose:
                print(
                    "Detected" if is_detected else "Rejected",
                    "with statistic", statistic, ":",
                    *obs,
                    file=sys.stderr,
                )
                sys.stderr.flush()

            # Keep sample if detected.
            if is_detected:
                # Transform to desired output coordinates.
                obs_out = self.transf_to_obs(obs)
                # Store in output arrays
                for dest, data in zip(observables, obs_out):
                    dest[n_generated] = data
                detection_stats[n_generated] = statistic
                # Record that we've successfully generated one more data point.
                n_generated += 1

        return observables, detection_stats


def next_pow_two(x):
    """
    Return the next (integer) power of two above `x`.
    """
    x2 = 1
    while x2 < x:
        x2 = x2 << 1
    return x2
