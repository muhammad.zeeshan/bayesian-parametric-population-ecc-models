import typing
import numpy

from pop_models.astro_models.coordinates import coordinates, transformations
from pop_models.coordinate import CoordinateSystem, CoordinateTransforms
from pop_models.detection import RandomForestDetection

__all__ = [
    "CBCRandomForestDetection",
]

class CBCRandomForestDetection(RandomForestDetection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            random_forest,
            transformations: CoordinateTransforms=transformations,
            truth: typing.Optional[numpy.ndarray]=None,
            backend="sklearn",
        ) -> None:
        super().__init__(
            coord_system=coord_system,
            random_forest=random_forest,
            transformations=transformations,
            truth=truth,
            backend=backend,
        )

    def save(self, filename):
        """
        Save this CBCRandomForestDetection to a file.
        """
        if self.backend == "sklearn":
            import pickle

            # Store the coordinate system names onto the random forest object
            # for proper pickling.
            if not hasattr(self.random_forest, "coord_system_names"):
                self.random_forest.coord_system_names = [
                    coord.name for coord in self.coord_system
                ]

            with open(filename, "wb") as rf_file:
                pickle.dump(self.random_forest, rf_file)
        else:
            raise KeyError("Unknown backend")

    @classmethod
    def load(cls, filename, backend="sklearn"):
        """
        Load a CBCRandomForestDetection from a file.
        """
        if backend == "sklearn":
            import warnings
            import pickle

            warnings.warn(
                "Loading a Random Forest from a pickle file. This is "
                "dangerous, or going to crash, if the version of python or "
                "scikit-learn is different than what produced the file."
            )

            with open(filename, "rb") as rf_file:
                random_forest = pickle.load(rf_file)
        else:
            raise KeyError("Unknown backend")

        coord_system = CoordinateSystem(*(
            coordinates[name] for name in random_forest.coord_system_names
        ))

        # Return appropriate CBCGaussianDetection object.
        return cls(coord_system, random_forest, backend=backend)
