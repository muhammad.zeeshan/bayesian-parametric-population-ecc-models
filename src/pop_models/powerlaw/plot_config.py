from __future__ import division, print_function

labels = {
    "rate": r"\mathcal{R}",
    "log_rate": r"\log\mathcal{R}",
    "alpha": r"\alpha",
    "m_min": r"m_{\mathrm{min}}",
    "m_max": r"m_{\mathrm{max}}",
    "M_max": r"M_{\mathrm{max}}",
}

units = {
    "rate": r"\mathrm{Gpc}^{-3} \, \mathrm{yr}^{-1}",
    "m_min": r"\mathrm{M}_{\odot}",
    "m_max": r"\mathrm{M}_{\odot}",
}
