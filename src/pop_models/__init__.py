__backend_name = "numpy"
__backend_locked = False
__valid_backends = ["numpy", "numba"]

__backend_already_set_message = """\
This call to pop_models.use() has no effect because the backend has already
been chosen; pop_models.use() must be called *before* any other submodules are
imported for the first time.\
"""

def use(backend, **backend_settings):
    """
    Set the backend to use for numerical calculations.
    """
    global __backend_name

    if backend not in __valid_backends:
        raise KeyError(
            "Unsupported backend {}.  Must be one of: {}"
            .format(backend, ", ".join(__valid_backends))
        )

    if __backend_locked:
        # Tried setting the backend when it was already locked in.
        import warnings
        warnings.warn(__backend_already_set_message)
    elif __backend_name == backend:
        # Nothing to do if the backend has already been set.
        pass
    else:
        # Set the backend to the desired value.
        __backend_name = backend


def get_backend():
    return __backend_name


def lock_backend():
    global __backend_locked
    __backend_locked = True
