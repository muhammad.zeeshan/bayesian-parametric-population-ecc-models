from . import (
    data_convert,
)

subparser_modules = [
    data_convert,
]

def populate_subparser(subparsers):
    subparser = subparsers.add_parser("O2RandPPaper")

    subsubparsers = subparser.add_subparsers(dest="command")

    for module in subparser_modules:
        module.populate_subparser(subsubparsers)
