__all__ = [
    "Coordinate",
    "CoordinateSystem",
    "CoordinateTransforms",
    "coord_nil",
    "transformations_nil",
]

import typing
from .types import Observables as _Observables
import numpy

class Coordinate(object):
    r"""
    Representation of a single coordinate, for use in
    :py:class:`CoordinateSystem` objects.  Requires a name, although it has no
    effect on its behaviour aside from how it's displayed.  Can optionally
    specify limits through the ``lower_limit`` and ``upper_limit`` arguments.
    Can also optionally specify ``fill_value``, which is meant to be used as a
    default value for potential uninitialized data, and should be a value the
    coordinate might possibly take, to avoid issues for code which processes it
    downstream.
    """
    def __init__(
            self,
            name: str,
            lower_limit: typing.Optional[float]=None,
            upper_limit: typing.Optional[float]=None,
            fill_value: float=numpy.nan,
        ):
        self.name = name
        self.lower_limit = lower_limit
        self.upper_limit = upper_limit
        self.fill_value = fill_value

    def __repr__(self) -> str:
        return "Coordinate({name})".format(name=self.name)


class CoordinateSystem(object):
    r"""
    Representation of an ordered set of :py:class:`Coordinate` objects.  Is used
    by various classes in PopModels to determine which observables to expect,
    and in which order, as well as for use in coordinate transformations,
    including through the :py:class:`CoordinateTransforms` class.
    """
    def __init__(self, *coords: Coordinate) -> None:
        self.coords = coords

    def __repr__(self) -> str:
        return (
            "CoordinateSystem({})"
            .format(", ".join(c.name for c in self.coords))
        )

    def __add__(self, coord_sys: "CoordinateSystem") -> "CoordinateSystem":
        return CoordinateSystem(*(self.coords + coord_sys.coords))

    def __eq__(self, coord_sys: object) -> bool:
        # If the other CoordinateSystem is not actually a CoordinateSystem,
        # they must not be equal.
        if not isinstance(coord_sys, CoordinateSystem):
            return False
        else:
            return self.coords == coord_sys.coords

    def equiv(self, coord_sys: "CoordinateSystem") -> bool:
        return set(self) == set(coord_sys)

    def issubset(self, coord_sys: "CoordinateSystem") -> bool:
        return set(self).issubset(coord_sys)

    def issuperset(self, coord_sys: "CoordinateSystem") -> bool:
        return set(self).issuperset(coord_sys)

    def __len__(self) -> int:
        return len(self.coords)

    def __iter__(self):
        return iter(self.coords)

    def __getitem__(self, index):
        return self.coords[index]

    def __hash__(self):
        return hash(self.coords)

    def __contains__(self, coord: Coordinate) -> bool:
        return coord in self.coords

    def index(self, coord: Coordinate):
        return self.coords.index(coord)

    def find_reordering(
            self,
            coord_sys: "CoordinateSystem",
        ) -> typing.Optional[typing.List[int]]:
        """
        Given another coordinate system, determines if the other is a simple
        re-ordering of itself.  If so, returns a list of indices, where the
        nth element gives the index in the alternate coordinate system that
        maps back to coordinate n in this system.  If there is no such
        reordering, returns None.
        """
        # One system has more coordinates than the other, so they can't be
        # re-orderings.
        if len(coord_sys) != len(self.coords):
            return None

        # Try to find the re-ordering.
        try:
            return [
                coord_sys.index(c)
                for c in self.coords
            ]
        # ValueError raised if a coordinate is missing, so this indicates they
        # aren't re-orderings.
        except ValueError:
            return None


# The empty coordinate system.
coord_nil = CoordinateSystem()

CoordinateSystemPair = typing.Tuple[CoordinateSystem, CoordinateSystem]
CoordinateTransformFunction = typing.Callable[[_Observables], _Observables]
CoordinateTransformMappings = typing.Dict[
    CoordinateSystemPair,
    CoordinateTransformFunction,
]
CoordinateTransformMappingsWithCost = typing.Dict[
    CoordinateSystemPair,
    typing.Tuple[float,CoordinateTransformFunction],
]
CorrelatedCoords = typing.FrozenSet[typing.FrozenSet[Coordinate]]
class CoordinateTransforms(object):
    r"""
    Represents a collection of known coordinate transformations, using mapping
    of coordinate system pairs to transformation functions, as well as
    additional transformations derived from that.

    Once constructed, you may index this object with a tuple of coordinate
    systems, and it will return the transformation function to go between them.
    If not possible, it will raise a :py:exc:`KeyError`.

    Currently supported derived transformations include...

    1) Reorderings:
    e.g., using knowledge of :math:`(x,y) \to (z)` to derive
    :math:`(y,x) \to (z)`.

    2) Combinations:
    e.g., using knowledge of :math:`(x,y) \to (z)` and
    :math:`(a,b) \to (c)` to derive :math:`(a,b,x,y) \to (c,z)`.

    3) Truncations:
    e.g., using knowledge of :math:`(x,y) \to (a,b)` to derive
    :math:`(x,y) \to (a)`.

    4) Truncated combinations:
    e.g., using knowledge of :math:`(x,y) \to (z)` and
    :math:`(a,b) \to (c,d)` to derive :math:`(a,b,x,y) \to (c,z)`.
    """
    def __init__(
            self,
            mappings: typing.Optional[CoordinateTransformMappings]=None,
        ) -> None:
        if mappings is None:
            mappings = {}

        self._pre_mappings = mappings
        self._mapping_computed = False

    def _setup_mappings(self) -> None:
        import time
        self._mappings = {} # type: CoordinateTransformMappingsWithCost
        self._mapping_computed = True

        for coord_system_pair, transform in self._pre_mappings.items():
            source_system, target_system = coord_system_pair

            test_data = self._make_test_data(source_system)
            t_start = time.time()
            transform(test_data)
            t_end = time.time()

            cost = t_end - t_start
            self._mappings[coord_system_pair] = (cost, transform)


    def _make_test_data(
            self,
            coord_system: CoordinateSystem,
        ) -> typing.Tuple[numpy.ndarray, ...]:
        test_data = [] # type: typing.List[numpy.ndarray]
        for i, coord in enumerate(coord_system):
            random_state = numpy.random.RandomState(i)

            lo = coord.lower_limit
            hi = coord.upper_limit

            # Make up some numbers if some limits don't exist, ensuring that
            # lo < hi.
            if (lo is None) and (hi is None):
                lo, hi = 0.0, 1.0
            elif lo is None:
                lo = -(hi + 1.0)
            elif hi is None:
                hi = -(lo - 1.0)

            test_data += (random_state.uniform(lo, hi, 1000000),)

        return tuple(test_data)


    def __contains__(
            self,
            coord_sys_pair: CoordinateSystemPair,
        ) -> bool:
        if not self._mapping_computed:
            self._setup_mappings()

       ## TODO: Optimize
        try:
            self[coord_sys_pair]
            return True
        except KeyError:
            return False

    def __getitem__(
            self,
            coord_sys_pair: CoordinateSystemPair,
        ) -> CoordinateTransformFunction:
        if not self._mapping_computed:
            self._setup_mappings()

        source_sys, target_sys = coord_sys_pair

        # Return the identity mapping if source and target are the same.
        if source_sys == target_sys:
            return identity_transform

        # Return the mapping if it exists exactly.
        try:
            cost, transform = self._mappings[coord_sys_pair]
            return transform
        except KeyError:
            pass

        # Check if the target system is a subset of the source system, and if
        # so, return a transformation function that downselects its inputs, and
        # ensures they are ordered as expected.
        if target_sys.issubset(source_sys):
            indices = [source_sys.index(coord) for coord in target_sys]
            def downselected_coord_transform_func(
                    observables: _Observables,
                ) -> _Observables:
                return tuple(observables[i] for i in indices)
            return downselected_coord_transform_func

        # Attempt to return the mapping with a change of order in the
        # coordinates.
        for candidate_pair, (cost, candidate_fn) in self._mappings.items():
            candidate_source_sys, candidate_target_sys = candidate_pair
            is_equiv = (
                source_sys.equiv(candidate_source_sys) and
                target_sys.equiv(candidate_target_sys)
            )
            if is_equiv:
                source_reordering = typing.cast(
                    typing.List[int],
                    source_sys.find_reordering(candidate_source_sys),
                )

                target_reordering = typing.cast(
                    typing.List[int],
                    candidate_target_sys.find_reordering(target_sys),
                )

                def reordered_coord_transform_func(
                        observables: _Observables,
                    ) -> _Observables:
                    transformed_observables_reordered = candidate_fn(
                        tuple(observables[i] for i in source_reordering)
                    )
                    return tuple(
                        transformed_observables_reordered[i]
                        for i in target_reordering
                    )

                return reordered_coord_transform_func

        # Attempt to combine multiple mappings, truncate a mapping, or both.
        # Strategy is to build a set of all possible mappings, i.e., those whose
        # inputs are all known, and whose combined outputs cover the desired
        # coordinates (possibly with extras to be truncated).  The lowest-cost
        # option is chosen.
        #
        # Start by reducing our considerations to mappings where the source
        # system only includes known input coordinates, and the target system
        # includes at least one desired output coordinate.
        possible_components = [
            (cand_source_sys, cand_target_sys, cost, transf)
            for (cand_source_sys, cand_target_sys), (cost, transf)
            in self._mappings.items()
            if (
                cand_source_sys.issubset(source_sys) and
                any(coord in cand_target_sys for coord in target_sys)
            )
        ]
        # Tack on identity mapping for each source coordinate, to allow for
        # simple pass-through of coordinates.  We consider this as zero-cost,
        # although in reality it has a small cost.
        possible_components += [
            (CoordinateSystem(coord), CoordinateSystem(coord), 0.0, lambda x: x)
            for coord in source_sys
            if coord in target_sys
        ]

        lowest_cost = numpy.inf
        lowest_cost_components = None
        # Convert the target system to a set, for re-use later.
        target_sys_set = set(target_sys)

        for components in _nonempty_powerset(possible_components):
            # Check if this is even a candidate.  The combined candidate target
            # coordinate systems must be a superset or equal to the desired
            # target coordinate system.
            cand_target_sys_set = set.union(*(
                set(cand_target_sys)
                for (_, cand_target_sys, _, _) in components
            ))
            if not target_sys_set.issubset(cand_target_sys_set):
                continue

            # Total cost is the sum of the costs for each component.
            total_cost = sum(cost for (_, _, cost, _) in components)

            # If the total cost beats the current lowest cost found, replace it.
            if total_cost < lowest_cost:
                lowest_cost = total_cost
                lowest_cost_components = components

        # So long as `lowest_cost_components` is no longer `None`, that means
        # we've found a solution.
        if lowest_cost_components is not None:
            # Pre-compute two lists to be re-used by the compound transformation
            # function any time it's called.
            #
            # First list is a list-of-lists.  One list for each component, and
            # each of those sub-lists will have one entry for each coordinate
            # in that component's source system, which will hold the index in
            # the desired source system to grab the value of.
            source_reorderings = [
                [
                    source_sys.index(coord)
                    for coord in comp_source_sys
                ]
                for (comp_source_sys, _, _, _) in lowest_cost_components
            ]
            # Second list is a list of 2-tuples, with one pair for each
            # coordinate in the target system.  Each pair tells the component
            # index to grab that target value from, as well as the index within
            # that component's values to grab.  If multiple components contain
            # the same value, we always take the first as listed in
            # `lowest_cost_components`, as it shouldn't matter which is used.
            target_reorderings = []
            for coord in target_sys:
                iterables = enumerate(lowest_cost_components)
                for i, (_, comp_target_sys, _, _) in iterables:
                    try:
                        j = comp_target_sys.index(coord)
                        target_reorderings.append((i, j))
                        break
                    except ValueError:
                        continue
            # Pre-compute a list of each of the component transformations, just
            # to avoid un-necessary unpacking.
            component_transforms = [
                transf for (_, _, _, transf) in lowest_cost_components
            ]

            # Define and return the compound transformation function.
            def compound_coord_transform_func(
                    observables: _Observables,
                ) -> _Observables:
                comp_transformed_observables = [
                    transf(tuple(observables[i] for i in reordering))
                    for transf, reordering
                    in zip(component_transforms, source_reorderings)
                ]

                return tuple(
                    comp_transformed_observables[i][j]
                    for (i, j) in target_reorderings
                )

            return compound_coord_transform_func


        raise KeyError(
            "No known transformation from {} to {}"
            .format(source_sys, target_sys)
        )

    def correlations(
            self,
            source_correlated_coords: CorrelatedCoords,
            source_sys: CoordinateSystem, target_sys: CoordinateSystem,
        ) -> CorrelatedCoords:
        """
        Takes a set of coordinates which are correlated in the source system
        and maps to the set of correlated coordinates in the target system.
        """
        # Initialize all target coordinates as candidates.
        candidates = {
            coord : {c for c in target_sys if c != coord}
            for coord in target_sys
        } # type: typing.Dict[Coordinate,typing.Set[Coordinate]]

        for coord_A in target_sys:
            for coord_B in candidates[coord_A].copy():
                pass


def _nonempty_powerset(iterable):
    from itertools import chain, combinations
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1, len(s)+1))

def identity_transform(observables: _Observables) -> _Observables:
    return observables

# The empty coordinate transformation set
transformations_nil = CoordinateTransforms()
