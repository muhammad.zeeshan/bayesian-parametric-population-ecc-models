"""
Routines for computing posterior predictive distributions.
"""

import typing

import numpy

from pop_models.population import Population
from pop_models.types import Observables, Parameters
from pop_models.utils import debug_verbose

class PPDSampler(object):
    """
    Samples from a population's posterior predictive distribution.
    """
    def __init__(
            self,
            population: Population,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ):
        if random_state is None:
            random_state = numpy.random.RandomState()

        self.population = population
        self.random_state = random_state

    def sample(
            self,
            n_samples: int,
            parameters: typing.Mapping[str, typing.Sequence],
            weights: typing.Optional[numpy.ndarray]=None,
            keep_params: bool=False, shuffle: bool=True,
        ) -> typing.Union[Observables, typing.Tuple[Observables, Parameters]]:
        # Check the number of parameter samples provided.
        n_post = numpy.size(next(iter(parameters.values())))

        # Initialize output observables
        observables = [
            numpy.empty(n_samples) for coord in self.population.coord_system
        ]
        # Initialize corresponding parameters if requested
        if keep_params:
            corresponding_parameters = {
                param_name : numpy.empty(n_samples)
                for param_name in parameters.keys()
            }

        # Determine probability of drawing using each posterior sample.
        # If ``post_weights`` is None, this is just 1/n_post, otherwise it will be
        # proportional to ``post_weights``, but properly normalized to 1.0.
        if weights is None:
            weights = numpy.broadcast_to(1.0/n_post, n_post)
        else:
            weights = weights / numpy.sum(weights)

        # Determine number of samples to draw from ``sampler`` for each sample in
        # ``post_samples``.
        counts = self.random_state.multinomial(n_samples, weights)

        # Draw samples from the PPD.
        n_drawn = 0
        for i, count in enumerate(counts):
            # Skip if count is zero
            if count == 0:
                debug_verbose(
                    "Drawing zero samples for hyperparameter", i,
                    mode="ppd_sampler", flush=True,
                )
                continue

            n_drawn_next = n_drawn + count
            out_slice = slice(n_drawn, n_drawn_next)
            # Extract parameters at index `i`.
            parameters_at_i = {
                param_name : numpy.asarray([values[i]])
                for param_name, values in parameters.items()
            }
            debug_verbose(
                "Drawing", count, "samples for hyperparameter", i,
                "with values", parameters_at_i,
                mode="ppd_sampler", flush=True,
            )
            # Draw the specified number of samples for this set of parameters.
            observables_at_i = self.population.rvs(
                count, parameters_at_i,
                random_state=self.random_state,
            )
            # Store results in output observables arrays.
            for obs_out, obs_sample in zip(observables, observables_at_i):
                obs_out[out_slice] = typing.cast(
                    typing.Tuple[numpy.ndarray,...],
                    obs_sample,
                )[0]
            # Store corresponding parameters if requested.
            if keep_params:
                for param_name in parameters.keys():
                    corresponding_parameters[param_name][out_slice] = (
                        parameters_at_i[param_name]
                    )

            # Bookkeeping
            n_drawn = n_drawn_next

        # Shuffle output observables if requested, and corresponding params if kept.
        if shuffle:
            idx_sort = self.random_state.permutation(n_samples)

            for i in range(len(observables)):
                observables[i] = observables[i][idx_sort]

            if keep_params:
                for param_name in parameters.keys():
                    corresponding_parameters[param_name] = (
                        corresponding_parameters[param_name][idx_sort]
                    )

        if keep_params:
            return tuple(observables), corresponding_parameters
        else:
            return tuple(observables)

    def sample_to_ascii(
            self,
            filename: str,
            n_samples: int,
            parameters: typing.Mapping[str, typing.Sequence],
            weights: typing.Optional[numpy.ndarray]=None,
            keep_params: bool=False, shuffle: bool=True,
        ) -> None:
        results = self.sample(
            n_samples,
            parameters,
            weights=weights,
            keep_params=keep_params, shuffle=shuffle,
        )

        header = (
            "# "+" ".join(coord.name for coord in self.population.coord_system)
        )
        if keep_params:
            observables, corresponding_parameters = typing.cast(
                typing.Tuple[
                    typing.Tuple[numpy.ndarray],
                    typing.Mapping[str, numpy.ndarray],
                ],
                results,
            )
            header += " "+" ".join(parameters.keys())
        else:
            observables = typing.cast(typing.Tuple[numpy.ndarray], results)

        with open(filename, "w") as ascii_file:
            print(header, file=ascii_file)

            for i in range(n_samples):
                # Write observables to file.
                print(*(obs[i] for obs in observables), end="", file=ascii_file)
                # Write parameters to file if kept.
                if keep_params:
                    print(
                        "",
                        *(val[i] for val in corresponding_parameters.values()),
                        end="",
                        file=ascii_file,
                    )
                # Write line seperator to file.
                print(file=ascii_file)
