r"""
This module defines all of the probability density functions and random sampling
functions for the power law mass distribution model.
"""

from __future__ import division, print_function

# Names of all (possibly free) parameters of this model, in the order they
# should appear in any array of samples (e.g., MCMC posterior samples).
param_names = [
    "log_rate",
    "alpha", "m_min", "m_max",
    "log_alpha_chi1", "log_beta_chi1", "log_alpha_chi2", "log_beta_chi2",
]
# Number of (possibly free) parameters for this population model.
ndim_pop = len(param_names)


def powerlaw_rvs(N, alpha, x_min, x_max, rand_state=None):
    r"""
    Draws ``N`` samples from a power law :math:`p(x) \propto x^{-\alpha}`, with
    support only betwen ``x_min`` and ``x_max``. Uses inverse transform
    sampling, drawing from the function

    .. math::
       \left((1-U) L + U H\right)^{1/\beta}

    where :math:`U` is uniformly distributed on (0, 1), and

    .. math::
       \beta = 1 - \alpha,
       L = x_{\mathrm{min}}^\beta,
       H = x_{\mathrm{max}}^\beta

    :param int N: Number of random samples to draw.

    :param float alpha: Power law index on :math:`p(x) \propto x^{-\alpha}`.

    :param float x_min: Lower limit of power law.

    :param float x_max: Upper limit of power law.

    :param numpy.random.RandomState rand_state: (optional) State for RNG.

    :return: array_like, shape (N,)
        Array of random samples drawn from distribution.
    """
    import numpy
    from ..utils import check_random_state

    # Upgrade ``rand_state`` to an actual ``numpy.random.RandomState`` object,
    # if it isn't one already.
    rand_state = check_random_state(rand_state)

    # Power law index ``alpha`` only appears as ``1 - alpha`` in the equations,
    # so define this quantity as ``beta`` and use it henceforth.
    beta = 1 - alpha

    # Uniform random samples between zero and one.
    U = rand_state.uniform(size=N)

    # x_{min,max}^beta, which appear in the inverse transform equation
    L = numpy.power(x_min, beta)
    H = numpy.power(x_max, beta)

    # Compute the random samples.
    return numpy.power((1-U)*L + U*H, 1.0/beta)


def joint_rvs(
        N,
        alpha, m_min, m_max, M_max,
        alpha_chi1, beta_chi1, alpha_chi2, beta_chi2,
        rand_state=None
    ):
    r"""
    Draws :math:`N` samples from the joint mass distribution :math:`p(m_1, m_2)`
    defined in :mod:`pop_models.powerlaw` as

    .. math::
       p(m_1, m_2) =
       C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
       \frac{m_1^{-\alpha}}{m_1 - m_{\mathrm{min}}}

    First draws samples from power law mass distribution :math:`p(m_1)`, then
    draws samples from the uniform distribution :math:`p(m_2 | m_1)`, and then
    rejects samples which do not satisfy the
    :math:`m_1 + m_2 \leq M_{\mathrm{max}}` constraint.
    """
    import numpy
    import scipy.stats

    from ..powerlaw.prob import joint_rvs as powerlaw_joint_rvs
    from ..utils import check_random_state

    rand_state = check_random_state(rand_state)

    m1_m2 = powerlaw_joint_rvs(
        N,
        alpha, m_min, m_max, M_max,
        rand_state=rand_state,
    )

    Beta_1 = scipy.stats.beta(alpha_chi1, beta_chi1) 
    Beta_2 = scipy.stats.beta(alpha_chi2, beta_chi2)

    chi_1 = Beta_1.rvs(N, random_state=rand_state)
    chi_2 = Beta_2.rvs(N, random_state=rand_state)

    return numpy.column_stack((m1_m2, chi_1, chi_2))


def joint_pdf(
        m_1, m_2, chi_1, chi_2,
        alpha, m_min, m_max, M_max,
        alpha_chi1, beta_chi1, alpha_chi2, beta_chi2,
        const=None,
        Beta_rv_chi1=None, Beta_rv_chi2=None,
    ):
    r"""
    Computes the probability density for the joint mass distribution
    :math:`p(m_1, m_2, \chi_1, \chi_2)` defined in
    :mod:`pop_models.powerlaw_spin_mag` as

    .. math::
       p(m_1, m_2, \chi_1, \chi_2) =
       C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
       \frac{m_1^{-\alpha}}{m_1 - m_{\mathrm{min}}} \,
       \mathrm{Beta}(\chi_1; \alpha_{\chi_1}, \beta_{\chi_1}) \,
       \mathrm{Beta}(\chi_2; \alpha_{\chi_2}, \beta_{\chi_2})

    Computes the normalization constant using :func:`pdf_const` if not provided
    by the ``const`` keyword argument.
    """
    import scipy.stats

    from ..powerlaw.prob import joint_pdf as powerlaw_joint_pdf

    if Beta_rv_chi1 is None:
        Beta_rv_chi1 = scipy.stats.beta(alpha_chi1, beta_chi1)
    if Beta_rv_chi2 is None:
        Beta_rv_chi2 = scipy.stats.beta(alpha_chi2, beta_chi2)

    return (
        powerlaw_joint_pdf(m_1, m_2, alpha, m_min, m_max, M_max, const=const) *
        Beta_rv_chi1.pdf(chi_1) * Beta_rv_chi2.pdf(chi_2)
    )


def marginal_m1_pdf(m1, alpha, m_min, m_max, M_max, const=None):
    r"""
    Computes the probability density for the marginal mass distribution
    :math:`p(m_1)` defined in :mod:`pop_models.powerlaw` as

    .. math::
       p(m_1, m_2) =
       C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
       m_1^{-\alpha} \,
       \frac{\min(m_1, M_{\mathrm{max}}-m_1) - m_{\mathrm{min}}}
            {m_1 - m_{\mathrm{min}}}

    Computes the normalization constant using :func:`pdf_const` if not provided
    by the ``const`` keyword argument.
    """
    from ..powerlaw.prob import marginal_pdf

    return marginal_pdf(m1, alpha, m_min, m_max, M_max, const=const)


def marginal_spin_mag_pdf(chi, alpha_chi, beta_chi):
    import numpy
    import scipy.stats

    from . import utils

    chi = numpy.asarray(chi)
    alpha_chis, beta_chis = utils.upcast_scalars((alpha_chi, beta_chi))

    pdf = numpy.zeros((len(alpha_chis), len(chi)), dtype=chi.dtype)

    for i, (alpha_chi, beta_chi) in enumerate(zip(alpha_chis, beta_chis)):
        pdf[i] = scipy.stats.beta(alpha_chi, beta_chi).pdf(chi)

    return pdf
