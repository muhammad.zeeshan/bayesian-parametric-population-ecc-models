"""
Routines for computing Savage-Dickey ratio for nested models.
"""
import typing

from pop_models.posterior import H5CleanedPosteriorSamples
from pop_models.types import Parameters

import numpy
from scipy.stats import gaussian_kde

import warnings
import json

def savage_dickey_ratio(
        param_samples_posterior: Parameters, param_samples_prior: Parameters,
        parameters_fixed: Parameters,
        density_method: str="kde",
        live_dangerously: bool=False,
    ) -> float:
    if not live_dangerously:
        if param_samples_posterior.keys() != param_samples_prior.keys():
            raise ValueError(
                "Posterior and prior do not have the same parameter names."
            )

    # Extract fixed parameter names and values into lists for re-use.
    param_names_fixed = list(parameters_fixed.keys())
    param_values_fixed = list(parameters_fixed.values())

    posterior_samples = [
        param_samples_posterior[param_name]
        for param_name in param_names_fixed
    ]
    prior_samples = [
        param_samples_prior[param_name]
        for param_name in param_names_fixed
    ]

    posterior_density = gaussian_kde(posterior_samples)
    prior_density = gaussian_kde(prior_samples)

    posterior_at_fixed = posterior_density(param_values_fixed)
    prior_at_fixed = prior_density(param_values_fixed)

    return posterior_density / prior_density


def savage_dickey_ratio_analytic_prior(
        posterior_samples: H5CleanedPosteriorSamples,
        parameters_fixed: Parameters,
        density_method: str="kde",
        log_scale: bool=True,
    ) -> float:
    # Load in description of prior distribution used.
    prior_settings = json.loads(
        posterior_samples.metadata["pop_prior_settings"]
    ) # type: typing.Dict[str, dict]

    # Initialize accumulator variable for the log(prior)
    log_prior_val = 0.0 # type: float

    # Accumulate contributions to the log(prior) value.
    for param_name, param_val in parameters_fixed.items():
        param_prior = prior_settings[param_name]

        if param_prior["dist"] == "uniform":
            prior_min = param_prior["params"]["min"]
            prior_max = param_prior["params"]["max"]

            if prior_min <= param_val <= prior_max:
                log_prior_val -= numpy.log(prior_max - prior_min)
            else:
                warnings.warn(
                    "Parameter {} = {} is not within the prior range [{},{}]"
                    .format(param_name, param_val, prior_min, prior_max)
                )
                log_prior_val = numpy.NINF

        elif param_prior["dist"] == "log-uniform":
            prior_min = param_prior["params"]["min"]
            prior_max = param_prior["params"]["max"]

            if prior_min <= param_val <= prior_max:
                log_prior_val -= numpy.log(param_val)
            else:
                warnings.warn(
                    "Parameter {} = {} is not within the prior range [{},{}]"
                    .format(param_name, param_val, prior_min, prior_max)
                )
                log_prior_val = numpy.NINF

        else:
            raise NotImplementedError(
                "Prior '{}' not yet implemented.".format(param_prior["dist"])
            )

    posterior_params = posterior_samples.get_params(...)

    posterior_samples_list = [
        posterior_params[param_name]
        for param_name in parameters_fixed.keys()
    ]

    if density_method == "kde":
        posterior_density = gaussian_kde(posterior_samples_list)
        log_posterior_val = numpy.log(
            posterior_density(list(parameters_fixed.values()))[0]
        )

    else:
        raise NotImplementedError(
            "Unknown density method: {}".format(density_method)
        )

    log_savage_dickey_ratio = log_posterior_val - log_prior_val

    if log_scale:
        return log_savage_dickey_ratio
    else:
        return numpy.exp(log_savage_dickey_ratio)


def make_parser():
    import argparse

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command")

    ## Analytic prior ##
    subparser_analytic_prior = subparsers.add_parser("analytic_prior")

    subparser_analytic_prior.add_argument(
        "posterior_samples",
        help="An HDF5 file containing posterior samples.",
    )
    subparser_analytic_prior.add_argument(
        "constants",
        help="A JSON file specifying constants which specify the nested model.",
    )

    subparser_analytic_prior.add_argument(
        "--density-estimator",
        default="kde", choices=["kde"],
        help="Density estimation method to use.",
    )

    subparser_analytic_prior.add_argument(
        "--log-scale",
        action="store_true", default=True,
        help="Return the natural logarithm of the Savage-Dickey ratio "
             "(default behavior)",
    )
    subparser_analytic_prior.add_argument(
        "--linear-scale",
        action="store_false", dest="log_scale",
        help="Return the Savage-Dickey ratio, instead of its logarithm.",
    )

    ## Sampled prior ##
    subparser_sampled_prior = subparsers.add_parser("sampled_prior")

    subparser_sampled_prior.add_argument(
        "posterior_samples",
        help="An HDF5 file containing posterior samples.",
    )
    subparser_sampled_prior.add_argument(
        "prior_samples",
        help="An HDF5 file containing prior samples.",
    )

    subparser_sampled_prior.add_argument(
        "constants",
        help="A JSON file specifying constants which specify the nested model.",
    )

    return parser


def main():
    cli_parser = make_parser()
    cli_args = cli_parser.parse_args()

    if cli_args.command == "analytic_prior":
        # Load in fixed values.
        with open(cli_args.constants, "r") as constants_file:
            parameters_fixed = json.load(constants_file)
        # Validate file formatted properly.
        if not isinstance(parameters_fixed, dict):
            raise ValueError(
                "Malformed constants file.  Must be a dictionary."
            )
        for k, v in parameters_fixed.items():
            if not (isinstance(k, str) and isinstance(v, float)):
                raise ValueError(
                    "Malformed constants file.  Must be a dictionary mapping "
                    "string parameter names to floating point values."
                )

        # Load in posterior samples.
        posterior_samples = H5CleanedPosteriorSamples(
            cli_args.posterior_samples
        )
        with posterior_samples:
            sd_ratio = savage_dickey_ratio_analytic_prior(
                posterior_samples,
                parameters_fixed,
                density_method=cli_args.density_estimator,
                log_scale=cli_args.log_scale,
            )
            print(sd_ratio)

    elif cli_args.command == "sampled_prior":
        raise NotImplementedError(
            "Using samples from the prior is not yet implemented."
        )

    else:
        raise KeyError(
            "Unknown command '{}' provided.".format(cli_args.command)
        )
