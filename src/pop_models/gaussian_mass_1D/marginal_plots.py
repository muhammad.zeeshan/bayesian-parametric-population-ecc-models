from __future__ import division, print_function, unicode_literals

def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "output_fmt",
        help="Format string which evaluates to filename to store plot in. "
             "Should have the string '{param}' in it, which will be replaced "
             "by each free parameter of the MCMC.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in each plot.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--fig-size",
        type=float, default=(8,8), nargs=2,
        help="Dimensions for figures.",
    )
    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import json
    import h5py
    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt
    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    from . import prob
    from . import plot_config
    from . import utils

    with h5py.File(args.posteriors, "r") as post_file:
        spins = post_file.attrs["spins"]
        param_names = prob.param_names(spins=spins)

        posteriors = post_file["pos"]

        pop_prior_settings = json.loads(post_file.attrs["priors"])
        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        params = utils.get_params(
            posteriors, constants, duplicates, param_names,
        )
        params = dict(zip(param_names, params))


        # Determine sample weights
        if (not args.rate_logU_to_jeffereys) or ("log10_rate" in constants):
            weights = None
        else:
            weights = numpy.sqrt(numpy.power(10.0, params["log10_rate"]))

        # Make plots
        for param_name in param_names:
            # Skip over constants and duplicates
            if (param_name in constants) or (param_name in duplicates):
                continue

            fig, ax = plt.subplots(figsize=args.fig_size)

            name = plot_pdf(
                ax,
                params[param_name], weights,
                param_name,
                args.n_samples,
                pop_prior_settings,
            )

            fig.tight_layout()
            fig.savefig(args.output_fmt.format(param=name))


def plot_pdf(ax, param, weights, param_name, n_samples, pop_prior_settings):
    return {
        "log10_rate": _plot_rate,
        "mu_m": _plot_mu_m,
        "log10_sigma_m": _plot_log10_sigma_m,
        "m_min": _plot_m_min,
        "m_max": _plot_m_max,
        "mu_chi": _plot_mu_chi,
        "sigma_sq_chi": _plot_sigma_sq_chi,
#        "alpha": _plot_alpha,
#        "beta": _plot_beta,
        "chi_min": _plot_chi_min,
        "chi_max": _plot_chi_max,
    }[param_name](ax, param, weights, n_samples, pop_prior_settings)


def _plot_rate(ax, log10_rate, weights, n_samples, pop_prior_settings):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    rate_label = plot_config.labels["rate"]
    rate_units = plot_config.units["rate"]

    # Determine plot limits from prior if possible, else use sample range.
    pop_prior_settings = pop_prior_settings["log10_rate"]
    dist = pop_prior_settings["dist"]
    if dist == "uniform":
        log_rate_min = pop_prior_settings["params"]["min"]
        log_rate_max = pop_prior_settings["params"]["max"]
    else:
        log_rate_min = numpy.floor(numpy.min(log10_rate))
        log_rate_max = numpy.ceil(numpy.max(log10_rate))

    log10_rate_smooth = numpy.linspace(log_rate_min, log_rate_max, n_samples)
    rate_smooth = numpy.power(10.0, log10_rate_smooth)

    dP_dlog10_rate = gaussian_kde(
        log10_rate, weights=weights,
        bw_method="scott",
    )
    dP_dlog10_rate_smooth = dP_dlog10_rate(log10_rate_smooth)

    dP_drate_smooth = dP_dlog10_rate_smooth / (rate_smooth * numpy.log(10.0))


    ax.semilogx(rate_smooth, rate_smooth*dP_drate_smooth)

    ax.set_xlabel(
        r"${rate_label} [{rate_units}]$"
        .format(rate_label=rate_label, rate_units=rate_units)
    )
    ax.set_ylabel(
        r"${rate_label} p({rate_label})$"
        .format(rate_label=rate_label)
    )

    return "rate"


def _plot_mu_m(*args):
    return _plot_simple("mu_m", *args)


def _plot_log10_sigma_m(*args):
    return _plot_simple("log10_sigma_m", *args)


def _plot_m_min(*args):
    return _plot_simple("m_min", *args)


def _plot_m_max(*args):
    return _plot_simple("m_max", *args)


def _plot_alpha(*args):
    return _plot_simple("alpha", *args)


def _plot_beta(*args):
    return _plot_simple("beta", *args)


def _plot_mu_chi(*args):
    return _plot_simple("mu_chi", *args)


def _plot_sigma_sq_chi(*args):
    return _plot_simple("sigma_sq_chi", *args)


def _plot_chi_min(*args):
    return _plot_simple("chi_min", *args)


def _plot_chi_max(*args):
    return _plot_simple("chi_max", *args)


def _plot_simple(name, ax, values, weights, n_samples, pop_prior_settings):
    import numpy
    from ..utils import gaussian_kde

    from . import plot_config

    label = plot_config.labels[name]


    # Determine plot limits from prior.
    val_min = pop_prior_settings[name]["params"]["min"]
    val_max = pop_prior_settings[name]["params"]["max"]

    val_smooth = numpy.linspace(val_min, val_max, n_samples)

    dP_dval = gaussian_kde(values, weights=weights, bw_method="scott")
    dP_dval_smooth = dP_dval(val_smooth)

    ax.plot(val_smooth, dP_dval_smooth)

    ax.set_xlabel(r"${label}$".format(label=label))
    ax.set_ylabel(r"$p({label})$".format(label=label))

    return name
