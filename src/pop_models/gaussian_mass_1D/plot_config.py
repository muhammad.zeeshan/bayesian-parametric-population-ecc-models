from __future__ import division, print_function, unicode_literals

labels = {
    "rate": r"\mathcal{R}",
    "log10_rate": r"\log_{10}\mathcal{R}",
    "mu_m": r"\mu_m",
    "sigma_sq_m": r"\sigma_m^2",
    "log10_sigma_m": r"\log_{10}(\sigma_m/\mathrm{M}_{\odot})",
    "m_min": r"m_{\mathrm{min}}",
    "m_max": r"m_{\mathrm{max}}",
    "alpha_chi": r"\alpha_{\chi}",
    "beta_chi": r"\beta_{\chi}",
    "mu_chi": r"\mu_{\chi}",
    "sigma_sq_chi": r"\sigma_{\chi}^2",
    "chi_min": r"\chi_{\mathrm{min}}",
    "chi_max": r"\chi_{\mathrm{max}}",
}

units = {
    "rate": r"\mathrm{Gpc}^{-3} \, \mathrm{yr}^{-1}",
    "mu_m": r"\mathrm{M}_{\odot}",
    "m_min": r"\mathrm{M}_{\odot}",
    "m_max": r"\mathrm{M}_{\odot}",
}
