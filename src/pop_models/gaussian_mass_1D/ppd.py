from __future__ import division, print_function, unicode_literals


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "ppd_samples",
        help="Text file to output PPD samples to.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to draw from PPD.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generator.",
    )


    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import h5py
    import numpy

    from . import prob, utils
    from .. import ppd

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    random_state = numpy.random.RandomState(args.seed)

    with h5py.File(args.posteriors, "r") as post_file:
        spins = post_file.attrs["spins"]

        param_names = prob.param_names(spins=spins)

        post_samples = post_file["pos"]

        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        n_post = len(post_samples)

        # Compute weights if different prior used.
        if args.rate_logU_to_jeffereys:
            log10_rates = utils.get_params(
                post_samples, constants, duplicates, param_names,
            )[0]
            if numpy.isscalar(log10_rates):
                log10_rates = numpy.tile(log10_rates, n_post)
            # Weights should be \sqrt(rate), which is 10^(0.5*log10(rate))
            post_weights = numpy.power(10.0, 0.5*log10_rates)
        else:
            post_weights = None


        def sampler(n_samples, post_sample, random_state=None):
            params = utils.get_params(
                post_sample, constants, duplicates, param_names,
            )

            return prob.joint_rvs(
                n_samples, params,
                rand_state=random_state,
                spins=spins,
            )

        ppd_samples = ppd.sample_ppd(
            sampler, args.n_samples,
            post_samples, post_weights=post_weights,
            random_state=random_state,
        )

        header_fields = ["m1_source", "m2_source"]
        if spins:
            header_fields += ["a1z", "a2z"]

        numpy.savetxt(
            args.ppd_samples, ppd_samples,
            delimiter="\t",
            header="\t".join(header_fields),
        )
