from __future__ import division, print_function, unicode_literals

color_1 = "#1f77b4"
color_2 = "#2ca02c"

def get_percentiles(data, weights=None, axis=0):
    import numpy
    from .. import utils

    percentiles = numpy.array([2.50, 16.0, 50.0, 84.0, 97.5])
    quantiles = percentiles / 100.0

    data_percentiles = utils.quantile(
        data, quantiles, weights=weights,
        axis=axis,
    )

    return {
        "median": data_percentiles[2],
        "1 sigma": (data_percentiles[1], data_percentiles[3]),
        "2 sigma": (data_percentiles[0], data_percentiles[4]),
    }

def plot_percentiles(ax, X, percentiles, color="black", alpha=0.25, label=None):
    for lo, hi in [percentiles["1 sigma"], percentiles["2 sigma"]]:
        ax.fill_between(
            X[1:-1], lo[1:-1], hi[1:-1],
            color=color, alpha=alpha,
        )
    ax.plot(
        X[1:-1], percentiles["median"][1:-1],
        color=color,
        label=label,
    )


def _get_args(raw_args):
    import argparse
    from . import prob

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "ppds",
        help="Text file containing PPD samples.",
    )
    parser.add_argument(
        "output_plot_dir",
        help="Directory to output plots into.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in plots.",
    )

    parser.add_argument(
        "--truth-overlay",
        nargs=len(prob.param_names), type=float,
        help="For mock data studies where the true model parameters are known, "
             "use this option to overlay the resulting values. Should be in "
             "order: {}"
             .format(prob.param_names),
    )
    parser.add_argument(
        "--truth-linestyle",
        default="dashed",
        help="Linestyle to use for truth overlay, if one is made.",
    )

    parser.add_argument(
        "--max-kde-samples",
        type=int,
        help="Maximum number of PPD samples to use when building a KDE. "
             "Use this if there are large memory concerns.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--log-scale-n-oom",
        type=int, default=6,
        help="Number of orders-of-magnitude below peak to show in log-scale "
             "plots.",
    )

    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generator.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    parser.add_argument(
        "--plot-format",
        default="png",
        help="File format to save plots as.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import os.path
    import sys
    import h5py
    import numpy
    import scipy.stats as stats

    from . import prob
    from . import utils
    from ..utils import empirical_distribution_function, gaussian_kde

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    import seaborn as sns

    matplotlib.rcParams["text.usetex"] = True
    matplotlib.rcParams["text.latex.preamble"] = [
        "\\usepackage{amsmath}",
        "\\usepackage{amssymb}",
    ]

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    rand_state = numpy.random.RandomState(args.seed)

    with h5py.File(args.posteriors, "r") as post_file:
        M_max = post_file.attrs["M_max"]
        iid_spins = post_file.attrs["iid_spins"]

        param_names = prob.param_names

        posteriors = post_file["pos"].value
        n_samples = len(posteriors)

        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        posteriors_dict = dict(zip(
            param_names,
            utils.get_params(
                posteriors, constants, duplicates, param_names,
            )
        ))


        m_min_abs = numpy.min(posteriors_dict["m_min"])
        m_max_abs = numpy.max(posteriors_dict["m_max"])

        # Temporary: will make these variables in the future
        chi_min_abs = 0.0
        chi_max_abs = 1.0

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(
                numpy.power(10.0, posteriors_dict["log10_rate"])
            )
            if numpy.isscalar(weights):
                weights = numpy.tile(weights, n_samples)
        else:
            weights = None


        ppd_samples = numpy.loadtxt(args.ppds)

        n_ppd_samples = len(ppd_samples)

        # If there are too many PPD samples, downselect, according to the limit
        # set by --max-kde-samples option.
        if args.max_kde_samples is None:
            pass
        elif n_ppd_samples <= args.max_kde_samples:
            pass
        else:
            idx_samples = rand_state.choice(
                n_ppd_samples, size=args.max_kde_samples,
                replace=False,
            )
            ppd_samples = ppd_samples[idx_samples]

        (
            m1_samples, m2_samples,
            chi1_samples, chi2_samples,
            costheta1_samples, costheta2_samples,
        ) = ppd_samples.T

        m_samples = numpy.concatenate((m1_samples, m2_samples))
        q_samples = m2_samples / m1_samples
        M_samples = m1_samples + m2_samples

        chi_eff_samples = (
            m1_samples*chi1_samples*costheta1_samples +
            m2_samples*chi2_samples*costheta2_samples
        ) / M_samples

        m = numpy.linspace(m_min_abs, m_max_abs, args.n_samples)
        q = numpy.linspace(m_min_abs/m_max_abs, 1.0, args.n_samples)
        M = 2*m

        chi = numpy.linspace(chi_min_abs, chi_max_abs, args.n_samples)
        chi_eff = numpy.linspace(-chi_max_abs, chi_max_abs, args.n_samples)
        costheta = chi_eff

        m1_pm1 = numpy.empty((n_samples, args.n_samples), dtype=numpy.float64)
        m1_R_pm1 = numpy.empty_like(m1_pm1)

        pchi1 = numpy.empty_like(m1_pm1)
        R_pchi1 = numpy.empty_like(m1_pm1)
        pcostheta1 = numpy.empty_like(m1_pm1)
        R_pcostheta1 = numpy.empty_like(m1_pm1)

        if args.truth_overlay is not None:
            (
                log10_rate_true,
                alpha_m_true, m_min_true, m_max_true,
                E_chi1_true, Var_chi1_true, mu_cos1_true, sigma_cos1_true,
                E_chi2_true, Var_chi2_true, mu_cos2_true, sigma_cos2_true,
            ) = args.truth_overlay

            rate_true = 10**log10_rate_true
            alpha_chi1_true, beta_chi1_true = (
                prob.spin_mu_sigma_sq_2_alpha_beta(E_chi1_true, Var_chi1_true)
            )
            alpha_chi2_true, beta_chi2_true = (
                prob.spin_mu_sigma_sq_2_alpha_beta(E_chi1_true, Var_chi2_true)
            )

            (
                m1_samples_true, m2_samples_true,
                chi1_samples_true, chi2_samples_true,
                costheta1_samples_true, costheta2_samples_true,
            ) = prob.joint_rvs(
                args.max_kde_samples,
                args.truth_overlay, M_max,
                rand_state=rand_state,
            ).T

            m_samples_true = numpy.concatenate(
                (m1_samples_true, m2_samples_true)
            )
            q_samples_true = m2_samples_true / m1_samples_true
            M_samples_true = m1_samples_true + m2_samples_true

            chi_eff_samples_true = (
                m1_samples_true*chi1_samples_true*costheta1_samples_true +
                m2_samples_true*chi2_samples_true*costheta2_samples_true
            ) / M_samples_true

            m1_pm1_true = m * prob.marginal_m1_pdf(
                m,
                alpha_m_true, m_min_true, m_max_true, M_max,
            )[0]
            m1_R_pm1_true = rate_true * m1_pm1_true

            pchi1_true = prob.marginal_spin_mag_pdf(
                chi, alpha_chi1_true, beta_chi1_true,
            )[0]
            R_pchi1_true = rate_true * pchi1_true

            pcostheta1_true = prob.marginal_spin_tilt_pdf(
                costheta, mu_cos1_true, sigma_cos1_true,
            )[0]
            R_pcostheta1_true = rate_true * pcostheta1_true

            if not iid_spins:
                pchi2_true = prob.marginal_spin_mag_pdf(
                    chi, alpha_chi2_true, beta_chi2_true,
                )[0]
                pchi2_true = rate_true * pchi2_true

                pcostheta2_true = prob.marginal_spin_tilt_pdf(
                    costheta, mu_cos2_true, sigma_cos2_true,
                )[0]
                R_pcostheta2_true = rate_true * pcostheta2_true


        if not iid_spins:
            pchi2 = numpy.empty_like(m1_pm1)
            R_pchi2 = numpy.empty_like(m1_pm1)
            pcostheta2 = numpy.empty_like(m1_pm1)
            R_pcostheta2 = numpy.empty_like(m1_pm1)

        for i, post_sample in enumerate(posteriors):
            weight = weights[i]

            (
                log10_rate,
                alpha_m, m_min, m_max,
                E_chi1, Var_chi1, mu_cos1, sigma_cos1,
                E_chi2, Var_chi2, mu_cos2, sigma_cos2,
            ) = utils.get_params(
                post_sample, constants, duplicates, param_names,
            )
            rate = numpy.power(10.0, log10_rate)
            alpha_chi1, beta_chi1 = (
                prob.spin_mu_sigma_sq_2_alpha_beta(E_chi1, Var_chi1)
            )
            alpha_chi2, beta_chi2 = (
                prob.spin_mu_sigma_sq_2_alpha_beta(E_chi1, Var_chi2)
            )


            pm1 = prob.marginal_m1_pdf(m, alpha_m, m_min, m_max, M_max)[0]

            m1_pm1[i] = m * pm1
            m1_R_pm1[i] = m * rate * pm1

            del pm1

            pchi1[i] = prob.marginal_spin_mag_pdf(
                chi, alpha_chi1, beta_chi1,
            )[0]
            R_pchi1[i] = rate * pchi1[i]
            if not iid_spins:
                pchi2[i] = prob.marginal_spin_mag_pdf(
                    chi, alpha_chi2, beta_chi2,
                )[0]
                R_pchi2[i] = rate * pchi2[i]

            pcostheta1[i] = prob.marginal_spin_tilt_pdf(
                costheta, mu_cos1, sigma_cos1,
            )[0]
            R_pcostheta1[i] = rate * pcostheta1[i]
            if not iid_spins:
                pcostheta2[i] = prob.marginal_spin_tilt_pdf(
                    costheta, mu_cos2, sigma_cos2,
                )[0]
                R_pcostheta2[i] = rate * pcostheta2[i]

        m1_pm1_percentile = get_percentiles(m1_pm1, weights=weights)
        m1_R_pm1_percentile = get_percentiles(m1_R_pm1, weights=weights)

        pchi1_percentile = get_percentiles(pchi1, weights=weights)
        R_pchi1_percentile = get_percentiles(R_pchi1, weights=weights)
        if not iid_spins:
            pchi2_percentile = get_percentiles(pchi2, weights=weights)
            R_pchi2_percentile = get_percentiles(R_pchi2, weights=weights)

        pcostheta1_percentile = get_percentiles(
            pcostheta1, weights=weights,
        )
        R_pcostheta1_percentile = get_percentiles(
            R_pcostheta1, weights=weights,
        )
        if not iid_spins:
            pcostheta2_percentile = get_percentiles(
                pcostheta2, weights=weights,
            )
            R_pcostheta2_percentile = get_percentiles(
                R_pcostheta2, weights=weights,
            )


        fig_m1_pm1, ax_m1_pm1 = plt.subplots()
        fig_m1_R_pm1, ax_m1_R_pm1 = plt.subplots()
        fig_ppd_mi, ax_ppd_mi = plt.subplots()
        fig_ppd_m, ax_ppd_m = plt.subplots()
        fig_ppd_q_M, ax_ppd_q_M = plt.subplots()

        fig_pchii, ax_pchii = plt.subplots()
        fig_R_pchii, ax_R_pchii = plt.subplots()
        fig_pcosthetai, ax_pcosthetai = plt.subplots()
        fig_R_pcosthetai, ax_R_pcosthetai = plt.subplots()
        fig_ppd_chii, ax_ppd_chii = plt.subplots()
        fig_ppd_costhetai, ax_ppd_costhetai = plt.subplots()
        fig_ppd_q_chieff, ax_ppd_q_chieff = plt.subplots()

        figs = [
            fig_m1_pm1,
            fig_m1_R_pm1,
            fig_ppd_mi,
            fig_ppd_m,
            fig_ppd_q_M,
            fig_pchii,
            fig_R_pchii,
            fig_pcosthetai,
            fig_R_pcosthetai,
            fig_ppd_chii,
            fig_ppd_costhetai,
            fig_ppd_q_chieff,
        ]

        axes_ppd_1d = [
            ax_ppd_mi,
            ax_ppd_m,
            ax_ppd_chii,
            ax_ppd_costhetai,
        ]

        # Logarithmic x-axes
        ax_logx = [
            ax_m1_pm1,
            ax_m1_R_pm1,
            ax_ppd_mi,
            ax_ppd_m,
        ]
        for ax in ax_logx:
            ax.set_xscale("log")

        # Logarithmic y-axes
        ax_logy = [
            ax_m1_pm1,
            ax_m1_R_pm1,
            ax_pchii,
            ax_R_pchii,
            ax_ppd_q_M,
        ]
        for ax in ax_logy:
            ax.set_yscale("log")

        # Limit oom for logarithmic percentile plots
        percentiles_pchii = [pchi1_percentile]
        percentiles_R_pchii = [R_pchi1_percentile]
        if not iid_spins:
            percentiles_pchii.append(pchi2_percentile)
            percentiles_R_pchii.append(R_pchi2_percentile)

        # NOTE: Should we add pcosthetai as well?
        ax_and_percentiles = [
            (ax_m1_pm1, [m1_pm1_percentile]),
            (ax_m1_R_pm1, [m1_R_pm1_percentile]),
            (ax_pchii, percentiles_pchii),
            (ax_R_pchii, percentiles_R_pchii),
        ]

        for ax, percentiles in ax_and_percentiles:
            ys = []
            for percentile in percentiles:
                ys += percentile["2 sigma"]

            ax.set_ylim(utils.limited_oom_range(ys, args.log_scale_n_oom))


        ## Label axes ##

        # x-axes
        for ax in [ax_m1_pm1, ax_m1_R_pm1]:
            ax.set_xlabel(r"$m_1 / \mathrm{M}_\odot$")

        ax_ppd_m.set_xlabel(r"$m / \mathrm{M}_\odot$")
        ax_ppd_mi.set_xlabel(r"$m_i / \mathrm{M}_\odot$")

        for ax in [ax_pchii, ax_R_pchii, ax_ppd_chii]:
            ax.set_xlabel(
                r"$\chi{}$"
                .format("" if iid_spins else "_i")
            )

        for ax in [ax_pcosthetai, ax_R_pcosthetai, ax_ppd_costhetai]:
            ax.set_xlabel(
                r"$\cos\theta{}$"
                .format("" if iid_spins else "_i")
            )

        for ax in [ax_ppd_q_chieff, ax_ppd_q_M]:
            ax.set_xlabel(r"$q$")


        # y-axes
        ax_m1_pm1.set_ylabel(
            r"$m_1 \, p(m_1)$"
        )
        ax_m1_R_pm1.set_ylabel(
            r"$m_1 \, \mathcal{R} \, p(m_1)$ [Gpc$^{-3}$ yr$^{-1}$]"
        )

        ax_ppd_q_chieff.set_ylabel(r"$\chi_{\mathrm{eff}}$")
        ax_ppd_q_M.set_ylabel(r"$M/\mathrm{M}_\odot$")

        ax_pchii.set_ylabel(
            r"$p(\chi{})$"
            .format("" if iid_spins else "_i")
        )
        ax_R_pchii.set_ylabel(
            r"$\mathcal{{R}} \, p(\chi{}) /"
            r" (\mathrm{{Gpc}}^{{-3}} \, \mathrm{{yr}}^{{-1}})$"
            .format("" if iid_spins else "_i")
        )
        ax_pcosthetai.set_ylabel(
            r"$p(\cos\theta{})$"
            .format("" if iid_spins else "_i")
        )
        ax_R_pcosthetai.set_ylabel(
            r"$\mathcal{{R}} \, p(\cos\theta{}) /"
            r" (\mathrm{{Gpc}}^{{-3}} \, \mathrm{{yr}}^{{-1}})$"
            .format("" if iid_spins else "_i")
        )

        for ax in axes_ppd_1d:
            ax.set_ylabel(r"$\mathrm{PPD}$")


        plot_percentiles(ax_m1_pm1, m, m1_pm1_percentile)
        plot_percentiles(ax_m1_R_pm1, m, m1_R_pm1_percentile)

        if args.truth_overlay is not None:
            ax_m1_pm1.plot(
                m, m1_pm1_true,
                color="black", linestyle=args.truth_linestyle,
            )
            ax_m1_R_pm1.plot(
                m, m1_R_pm1_true,
                color="black", linestyle=args.truth_linestyle,
            )

        if iid_spins:
            plot_percentiles(ax_pchii, chi, pchi1_percentile)
            plot_percentiles(ax_R_pchii, chi, R_pchi1_percentile)

            plot_percentiles(ax_pcosthetai, costheta, pcostheta1_percentile)
            plot_percentiles(ax_R_pcosthetai, costheta, R_pcostheta1_percentile)

            if args.truth_overlay is not None:
                ax_pchii.plot(
                    chi, pchi1_true,
                    color="black", linestyle=args.truth_linestyle,
                )
                ax_R_pchii.plot(
                    chi, R_pchi1_true,
                    color="black", linestyle=args.truth_linestyle,
                )

                ax_pcosthetai.plot(
                    costheta, pcostheta1_true,
                    color="black", linestyle=args.truth_linestyle,
                )
                ax_R_pcosthetai.plot(
                    costheta, R_pcostheta1_true,
                    color="black", linestyle=args.truth_linestyle,
                )
        else:
            plot_percentiles(
                ax_pchii, chii, pchi1_percentile,
                color=color_1,
            )
            plot_percentiles(
                ax_pchii, chii, pchi2_percentile,
                color=color_2,
            )

            plot_percentiles(
                ax_R_pchii, chii, R_pchi1_percentile,
                color=color_1,
            )
            plot_percentiles(
                ax_R_pchii, chii, R_pchi2_percentile,
                color=color_2,
            )

            plot_percentiles(
                ax_pcosthetai, costheta, pcostheta1_percentile,
                color=color_1,
            )
            plot_percentiles(
                ax_pcosthetai, costheta, pcostheta2_percentile,
                color=color_2,
            )

            plot_percentiles(
                ax_R_pcosthetai, costheta, R_pcostheta1_percentile,
                color=color_1,
            )
            plot_percentiles(
                ax_R_pcosthetai, costheta, R_pcostheta2_percentile,
                color=color_2,
            )

            if args.truth_overlay is not None:
                ax_pchii.plot(
                    chi, pchi1_true,
                    color=color_1, linestyle=args.truth_linestyle,
                )
                ax_pchii.plot(
                    chi, pchi2_true,
                    color=color_2, linestyle=args.truth_linestyle,
                )

                ax_R_pchii.plot(
                    chi, R_pchi1_true,
                    color=color_1, linestyle=args.truth_linestyle,
                )
                ax_R_pchii.plot(
                    chi, R_pchi2_true,
                    color=color_2, linestyle=args.truth_linestyle,
                )

                ax_pcosthetai.plot(
                    costheta, pcostheta1_true,
                    color=color_1, linestyle=args.truth_linestyle,
                )
                ax_pcosthetai.plot(
                    costheta, pcostheta2_true,
                    color=color_2, linestyle=args.truth_linestyle,
                )

                ax_R_pcosthetai.plot(
                    costheta, R_pcostheta1_true,
                    color=color_1, linestyle=args.truth_linestyle,
                )
                ax_R_pcosthetai.plot(
                    costheta, R_pcostheta2_true,
                    color=color_2, linestyle=args.truth_linestyle,
                )


        # Plot PPDs

        iterables = [
            (m1_samples, m2_samples, "solid"),
        ]
        if args.truth_overlay is not None:
            iterables.append(
                (m1_samples_true, m2_samples_true, args.truth_linestyle)
            )
        for m1_s, m2_s, ls in iterables:
            ppd_m1 = gaussian_kde(m1_s, bw_method="scott")(m)
            ppd_m2 = gaussian_kde(m2_s, bw_method="scott")(m)

            ax_ppd_mi.plot(m, ppd_m1, color=color_1, linestyle=ls)
            ax_ppd_mi.plot(m, ppd_m2, color=color_2, linestyle=ls)

            del m1_s, m2_s, ppd_m1, ppd_m2


        iterables = [
            (m_samples, "solid"),
        ]
        if args.truth_overlay is not None:
            iterables.append((m_samples_true, args.truth_linestyle))
        for m_s, ls in iterables:
            ppd_m = gaussian_kde(m_s, bw_method="scott")(m)
            ax_ppd_m.plot(m, ppd_m, color="black", linestyle=ls)

            del m_s, ppd_m


        q_mesh, M_mesh = numpy.meshgrid(q, M)

        ppd_q_M = (
            gaussian_kde([q_samples, M_samples], bw_method="scott")
        )([q_mesh.ravel(), M_mesh.ravel()]).reshape(q_mesh.shape)

        fig_ppd_q_M.colorbar(
            ax_ppd_q_M.contourf(q, M, ppd_q_M, 5),
            ax=ax_ppd_q_M,
        ).set_label(r"$\mathrm{PPD}$")

        if args.truth_overlay is not None:
            ppd_q_M = (
                gaussian_kde(
                    [q_samples_true, M_samples_true],
                    bw_method="scott",
                )
            )([q_mesh.ravel(), M_mesh.ravel()]).reshape(q_mesh.shape)
            ax_ppd_q_M.contour(
                q, M, ppd_q_M,
                5, colors="white", linewidths=0.75,
            )

        del q_mesh, M_mesh, ppd_q_M

        ppd_chi1 = gaussian_kde(chi1_samples, bw_method="scott")(chi)
        ppd_costheta1 = (
            gaussian_kde(costheta1_samples, bw_method="scott")(costheta)
        )

        if iid_spins:
            ax_ppd_chii.plot(chi, ppd_chi1, color="black")
            ax_ppd_costhetai.plot(costheta, ppd_costheta1, color="black")
        else:
            ppd_chi2 = gaussian_kde(chi2_samples, bw_method="scott")(chi)
            ppd_costheta2 = (
                gaussian_kde(costheta2_samples, bw_method="scott")(costheta)
            )

            ax_ppd_chii.plot(chi, ppd_chi1, color=color_1)
            ax_ppd_chii.plot(chi, ppd_chi2, color=color_2)

            ax_ppd_costhetai.plot(chi, ppd_costheta1, color=color_1)
            ax_ppd_costhetai.plot(chi, ppd_costheta2, color=color_2)


            del ppd_chi2z, ppd_costheta2
        del ppd_chi1, ppd_costheta1

        if args.truth_overlay is not None:
            ppd_chi1 = gaussian_kde(chi1_samples_true, bw_method="scott")(chi)
            ppd_costheta1 = (
                gaussian_kde(
                    costheta1_samples_true,
                    bw_method="scott",
                )(costheta)
            )

            if iid_spins:
                ax_ppd_chii.plot(
                    chi, ppd_chi1,
                    color="black", linestyle=args.truth_linestyle,
                )
                ax_ppd_costhetai.plot(
                    costheta, ppd_costheta1,
                    color="black", linestyle=args.truth_linestyle,
                )
            else:
                ppd_chi2 = gaussian_kde(
                    chi2_samples_true,
                    bw_method="scott",
                )(chi)
                ppd_costheta2 = gaussian_kde(
                    costheta2_samples_true,
                    bw_method="scott",
                )(costheta)

                ax_ppd_chii.plot(
                    chi, ppd_chi1,
                    color=color_1, linestyle=args.truth_linestyle,
                )
                ax_ppd_chii.plot(
                    chi, ppd_chi2,
                    color=color_2, linestyle=args.truth_linestyle,
                )

                ax_ppd_costhetai.plot(
                    chi, ppd_costheta1,
                    color=color_1, linestyle=args.truth_linestyle,
                )
                ax_ppd_costhetai.plot(
                    chi, ppd_costheta2,
                    color=color_2, linestyle=args.truth_linestyle,
                )


                del ppd_chi2z, ppd_costheta2
            del ppd_chi1, ppd_costheta1


        q_mesh, chieff_mesh = numpy.meshgrid(q, chi_eff)

        ppd_q_chieff = (
            gaussian_kde([q_samples, chi_eff_samples], bw_method="scott")
        )([q_mesh.ravel(), chieff_mesh.ravel()]).reshape(q_mesh.shape)

        fig_ppd_q_chieff.colorbar(
            ax_ppd_q_chieff.contourf(q, chi_eff, ppd_q_chieff, 5),
            ax=ax_ppd_q_chieff,
        ).set_label(r"$\mathrm{PPD}$")

        if args.truth_overlay is not None:
            ppd_q_chieff = (
                gaussian_kde(
                    [q_samples_true, chi_eff_samples_true],
                    bw_method="scott",
                )
            )([q_mesh.ravel(), chieff_mesh.ravel()]).reshape(q_mesh.shape)

            ax_ppd_q_chieff.contour(
                q, chi_eff, ppd_q_chieff,
                5, colors="white", linewidths=0.75,
            )

        del q_mesh, chieff_mesh, ppd_q_chieff


        for ax in [ax_m1_pm1, ax_m1_R_pm1]:
            ax.set_xlim([m_min_abs, m_max_abs])

        for ax in [ax_pchii, ax_R_pchii]:
            ax.set_xlim([chi_min_abs, chi_max_abs])

        for ax in [ax_pcosthetai, ax_R_pcosthetai]:
            ax.set_xlim([-1.0, +1.0])


        for fig in figs:
            fig.tight_layout()

        def plot_fname(basename):
            return os.path.join(
                args.output_plot_dir,
                ".".join([basename, args.plot_format])
            )

        fig_m1_pm1.savefig(plot_fname("dist_m1_pm1"))
        fig_m1_R_pm1.savefig(plot_fname("dist_m1_R_pm1"))

        fig_ppd_mi.savefig(plot_fname("ppd_mi"))
        fig_ppd_m.savefig(plot_fname("ppd_m"))

        fig_ppd_q_M.savefig(plot_fname("ppd_q_M"))

        fig_pchii.savefig(plot_fname(
            "dist_pchi{}"
            .format("" if iid_spins else "i")
        ))
        fig_R_pchii.savefig(plot_fname(
            "dist_R_pchi{}"
            .format("" if iid_spins else "i")
        ))

        fig_pcosthetai.savefig(plot_fname(
            "dist_pcostheta{}"
            .format("" if iid_spins else "i")
        ))
        fig_R_pcosthetai.savefig(plot_fname(
            "dist_R_pcostheta{}"
            .format("" if iid_spins else "i")
        ))

        fig_ppd_chii.savefig(plot_fname(
            "ppd_chi{}"
            .format("" if iid_spins else "i")
        ))
        fig_ppd_costhetai.savefig(plot_fname(
            "ppd_costheta{}"
            .format("" if iid_spins else "i")
        ))

        fig_ppd_q_chieff.savefig(plot_fname("ppd_q_chieff"))
