# Bayesian Parametric Population Models

This package provides techniques for inferring the merger rate density for compact binary sources, under the assumption of a particular population model, using Markov chain Monte Carlo. There is a very generic set of foundational tools provided, along with specific models built on top of those tools. [Click here for complete documentation](http://bayesian-parametric-population-models.readthedocs.io/en/latest/), [here for installation instructions](http://bayesian-parametric-population-models.readthedocs.io/en/latest/user_guide/install.html), and [here for API documentation](http://bayesian-parametric-population-models.readthedocs.io/en/latest/api/index.html).

At the heart of it, this is a package to sample from the posterior of population parameters $`\Lambda`$ for a merger rate density as a function of system parameters $`\lambda`$, via the likelihood

```math
\mathcal{L}(\Lambda) \propto
e^{-\mu(\Lambda)}
\prod_{n=1}^N
  \int \ell_n(\lambda) \rho(\lambda\mid\Lambda) \mathrm{d}\lambda
```

where $`\rho(\lambda\mid\Lambda) = \mathrm{d}N / (\mathrm{d}V \mathrm{d}t \mathrm{d}\lambda)`$ is the merger rate density for a population parameterized by $`\Lambda`$, $`\mu(\Lambda)`$ is the expected number of detected mergers for that population, and $`\ell_n(\lambda)`$ is the likelihood for the $`n`$th observed event's parameters. Using Bayes' theorem, we can obtain the posterior $`p(\Lambda\mid\text{data})`$ by multiplying the likelihood by a prior $`\pi(\Lambda)`$

```math
p(\Lambda\mid\text{data}) \propto
\pi(\Lambda) \mathcal{L}(\Lambda)
```

We obtain samples from this posterior using the implementation of Goodman & Weare’s Affine Invariant Markov chain Monte Carlo Ensemble sampler, provided by the Python package [emcee](http://dfm.io/emcee/).

The core API allows one to provide user-defined functions for $`\rho(\lambda\mid\Lambda)`$ and $`\mu(\Lambda)`$ and $\pi(\Lambda)$, as well as an array of samples $`\{ \lambda_{n,i} \}_{i=1}^{N_{\mathrm{samples}}}`$ drawn from the posterior of each of the $N$ events, $`p(\lambda | \mathrm{data}_n)`$, and the values of the reference prior used, $`\{ \pi_{n,i} \}_{i=1}^{N_{\mathrm{samples}}}`$. The integral inside the main likelihood expression is then evaluated via Monte Carlo as

```math
\int \ell_n(\lambda) \rho(\lambda\mid\Lambda) \mathrm{d}\lambda \propto
\int \frac{p(\lambda | \mathrm{data}_n)}{\pi_n(\lambda)} \rho(\lambda\mid\Lambda) \mathrm{d}\lambda \approx
\frac{1}{N_{\mathrm{samples}}}
\sum_{i=1}^{N_{\mathrm{samples}}} \frac{\rho(\lambda_{n,i}\mid\Lambda)}{\pi_{n,i}}
```

## Credit

Authors: Daniel Wysocki and Richard O'Shaughnessy

A paper is being written, but in the meantime you can cite this repository directly:

Wysocki D., O'Shaughnessy R., **Bayesian Parametric Population Models**, 2017–, [bayesian-parametric-population-models.readthedocs.io](http://bayesian-parametric-population-models.readthedocs.io) [Online; accessed YYYY-MM-DD].

```
@Misc{popmodels,
  author = {Daniel Wysocki and Richard O'Shaughnessy},
  title = {Bayesian Parametric Population Models},
  year = {2017--},
  url = "bayesian-parametric-population-models.readthedocs.io",
  note = {[Online; accessed YYYY-MM-DD]}
}
```