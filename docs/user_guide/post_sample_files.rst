Posterior Sample Files
======================

PopModels provides two file formats for storing posterior samples:

1. :py:class:`pop_models.posterior.H5RawPosteriorSamples` for raw sampler chains
2. :py:class:`pop_models.posterior.H5CleanedPosteriorSamples` for post-processed
   chains including things like burnin/auto-correlation removal

Both formats use the _HDF5 format as a backend.
:py:class:`pop_models.posterior.H5RawPosteriorSamples` is broken across multiple
files, with a 'master' file containing pointers to 'subfiles'.  This allows the
file to grow as more posterior samples are collected, so it should be used for
any Monte Carlo samplers which can be arbitrarily extended in sample size.
:py:class:`pop_models.posterior.H5CleanedPosteriorSamples` is fixed in size, and
is intended as a final product, to be produced from a
:py:class:`pop_models.posterior.H5RawPosteriorSamples` file via post-processing.


Raw posterior files
-------------------

Setup
.....

To set up a raw posterior file, first import the appropriate class

.. code-block:: python

   from pop_models.posterior import H5RawPosteriorSamples

In order to create a file, you will at the very least need an empty directory,
which by convention we call ``post_raw/``, a tuple of parameter names in the
order they should appear in the stored array, and an array storing the initial
state, where rows correspond to different chains, and columns correspond to the
parameters.

For this demo, we will consider a 2-D uniform distribution, which is uniform in
:math:`x` from 0 to 1, and in :math:`y` from -1 to 0.  Rather than use something
like MCMC, we will just sample directly using NumPy routines

.. code-block:: python

   import numpy as np
   def sample_posterior(N):
       "Draws N samples from our distribution"
       return np.column_stack((
         np.random.uniform( 0, +1, N),
         np.random.uniform(-1,  0, N),
       ))

Now we will make an initial state array by drawing one sample for each of our 8
chains.

.. code-block:: python

   n_walkers = 8
   init_state = sample_posterior(n_walkers)

Now we store our other configuration parameters as named variables for clarity.

.. code-block:: python

   dirname = "post_raw/"
   param_names = ("x", "y")

And with all this we can initialize our file.

.. code-block:: python

   post_samples = H5RawPosteriorSamples.create(dirname, param_names, init_state)

When you are finished using the file, be call the
:py:meth:`pop_models.posterior.H5RawPosteriorSamples.close` method to ensure it
gets flushed/closed properly.  To guarantee this, it is recommended to always
open it using a ``with`` statement as such:

.. code-block:: python

   # Open the file.
   post_samples = H5RawPosteriorSamples.create(dirname, param_names, init_state)
   # File is protected while inside this block, and is guaranteed to be closed
   # safely so long as Python doesn't exit unexpectedly.
   with post_samples:
       # Do stuff with the open file
       ...

   # File is now closed automatically when indented code finished, even if an
   # exception occurred.

However, if you are doing this tutorial interactively, the ``with`` statement
will be overkill and make it harder to experiment, so we recommend leaving it
out while you follow along.

Once you have closed the file, and want to use it again, you can open it like
so:

.. code-block:: python

   post_samples = H5RawPosteriorSamples(dirname, mode)

with ``mode="r"`` for read-only mode, and ``mode="r+"`` for read/write mode.



Manual use
..........

If you use a built-in sampler, like :py:class:`PopulationInferenceEmceeSampler`,
you will only need to open/close the file and not worry about how it works.
However, if you are developing your own sampler class, or just want to write
samples manually, you should read this section.

After opening a file for writing (described in previous section), one can append
posterior samples for all ``n_walkers`` chains with the
:py:meth:`pop_models.posterior.H5RawPosteriorSamples.append` method.  This takes
3 arguments: the sample locations, and the associated log posterior and prior
values.  The sample array should have shape ``(n_walkers, n_params)``, and the
probability arrays should have shape ``(n_walkers,)``.  The posterior and prior
do not need to be properly normalized.  Since our posterior is uniform, we will
fix the log probabilities to ``0.0``, and we will choose to use a uniform prior
as well, making those ``0.0`` too.

.. code-block:: python

   sample = sample_posterior(n_walkers)
   post_log_prob = prior_log_prob = np.zeros(n_walkers)

   post_samples.append(sample, post_log_prob, prior_log_prob)

Every time your sampler accumulates another step's samples, simply call
:py:meth:`pop_models.posterior.H5RawPosteriorSamples.append` again with the new
samples.



.. _HDF5: https://www.hdfgroup.org/solutions/hdf5/
