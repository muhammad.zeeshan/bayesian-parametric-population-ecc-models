BBH Mixture Population with Powerlaw and Gaussian Mass Model
============================================================

.. argparse::
    :module: pop_models.applications.bbh.mixture_pl_gauss.main
    :func: make_parser
    :prog: pop_models_bbh_mixture_pl_gauss
