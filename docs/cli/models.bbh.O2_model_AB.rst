BBH Mass Models A and B from LIGO/Virgo O2 R&P Paper
====================================================

.. argparse::
    :module: pop_models.applications.bbh.O2_model_AB.main
    :func: make_parser
    :prog: pop_models_bbh_O2_model_AB
