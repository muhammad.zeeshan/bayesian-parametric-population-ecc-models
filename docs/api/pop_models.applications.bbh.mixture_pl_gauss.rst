pop_models.applications.bbh.mixture_pl_gauss module
===================================================

.. automodule:: pop_models.applications.bbh.mixture_pl_gauss
    :members:
    :undoc-members:
    :show-inheritance:
