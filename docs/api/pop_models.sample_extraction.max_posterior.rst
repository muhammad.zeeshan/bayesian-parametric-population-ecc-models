pop_models.sample_extraction.max_posterior module
====================================================

.. automodule:: pop_models.sample_extraction.max_posterior
    :members:
    :undoc-members:
    :show-inheritance:
